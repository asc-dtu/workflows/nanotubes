from ase.calculators.vasp import Vasp2
from ase.io import read,write
import numpy as np
import pylab as plt
import pickle
import os,sys
from helper_functions import get_band_edge_parameters, get_bandgap, get_tube_bandstructure, get_ribbon_bandstructure, get_dos, fold_dos, get_averaged_hartree_potential_tube, get_averaged_hartree_potential_sheet, get_work_function_tube, get_work_function_sheet
from pathlib import Path
from nts.nanotube_tools import measure_diameter
import re
import warnings
from more_itertools import sort_together




def plot_bandstructure(path, emin, emax, reference = 'fermi_level', plot_tube = True, plot_ribbon = True, plot_tube_Fermi_level = True, show = True):


    #--------------------- Nanotube band structure -------------------------
    if plot_tube:

        kpoints, energies, nbands = get_tube_bandstructure(path)
        

        _, __, Ef, ___ = get_band_edge_parameters(path.split('bandstructure')[0] + 'fine_k_mesh') #get fermi level from calculation in k_mesh folder

        if reference == 'fermi_level':
            ref = Ef
        elif reference == 'vacuum_level':
            _, __, evac = get_averaged_hartree_potential_tube(path.split('bandstructure')[0] + 'fine_k_mesh') #get vacuum level from calculation in k_mesh folder
            ref = evac

        energies = energies - ref

        for i in range(nbands):
            plt.plot(kpoints, energies[0,:,i], color = 'b')

        if plot_tube_Fermi_level:
            plt.plot(kpoints, (Ef - ref)*np.ones(len(kpoints)), 'k--')

        emin = (Ef - ref) + emin
        emax = (Ef - ref) + emax


    #--------------------- Ribbon band structure -------------------------
    if plot_ribbon:
        
        #Get path for ribbon folder
        ribbon_path = path.split('/bandstructure')[0] + '/ribbon'

        #Get ribbon band structure
        kpoints, energies, nbands = get_ribbon_bandstructure(ribbon_path)


        if reference == 'fermi_level':
            _, __, Ef, ___ = get_band_edge_parameters(ribbon_path) 
            ref = Ef
        elif reference == 'vacuum_level':
            _, __, evac1, evac2 = get_averaged_hartree_potential_sheet(ribbon_path) 
            ref = evac2


        # Finally plot band structure
        for i in range(nbands):
            plt.plot(kpoints, energies[:,i]-ref, color = 'b', linestyle = '--')

 

    #--------------------- Plot options -------------------------
    plt.xlim(0.0, 0.5)
    plt.ylim((emin, emax))
    plt.ylabel('Energy [eV]')
    if show:
        plt.show()




def plot_bandgap_vs_diameter(path, plot_indirect = True, plot_direct = True, plot_2D = True, show = True):

   
    #--------------------- Start by retrieving and plotting 2D bandgap -------------------------
    base_path, _ = os.path.split(path[0:-1])
    path_2D = base_path + '/2DSheet/fine_k_mesh'

    try:
        bandgap_2D = get_bandgap(path_2D)
        bandgap_2D_direct = get_bandgap(path_2D, direct = True)

    except FileNotFoundError:
        print('No dense k mesh calculation for 2D sheet.')
   
 


    #--------------------- Now get band gaps for the available tube sizes -------------------------
    tube_sizes = os.listdir(path)
       
    diameters = []
    bandgaps = []
    bandgaps_direct = []
    for tube_size in tube_sizes:
        
        try:
            kmesh_path = path + '/' + tube_size + '/nm/fine_k_mesh'
            atoms = read(kmesh_path + '/OUTCAR')
            diameter = measure_diameter(atoms)
            bandgap = get_bandgap(kmesh_path)
            bandgap_direct = get_bandgap(kmesh_path, direct = True)

        except:
            print('No dense k mesh calculation for tube size {}.'.format(tube_size))
            continue
        
        diameters.append(diameter)
        bandgaps.append(bandgap)
        bandgaps_direct.append(bandgap_direct)

    diameters, bandgaps, bandgaps_direct = sort_together([diameters, bandgaps, bandgaps_direct])

    #--------------------- Plot results -------------------------
    if plot_indirect:
        plt.plot(diameters, bandgaps, Linestyle = '-', Marker = '*', label = 'Indirect band gap')
    if plot_direct:
        plt.plot(diameters, bandgaps_direct, Linestyle = '-', Marker = 'o', label = 'Direct band gap')
    if plot_2D:
        plt.plot(diameters, bandgap_2D * np.ones(len(diameters)), 'k--', label = '2D band gap')
        
    plt.legend()
    plt.xlabel('Diameter [$\AA$]')
    plt.ylabel('Band gap [eV]')
    if show:
        plt.show()



def plot_dos(path, pdos_to_plot, emin, emax, dos_min, dos_max, width = 0.05, npoints = 1000, shift = 0, reference = 'fermi_level', normalize = True, plot_total_dos = True, plot_band_edges = True, show = True):

    if len(pdos_to_plot)>0 and plot_total_dos and normalize:
        warnings.warn('It does not make sense to plot normalized dos and pdos together.')

    #Get dos dictionary and band edge parameters
    dos = get_dos(path = path, normalize = normalize)
    Ec, Ev, Ef, state = get_band_edge_parameters(path)

    #Determine material and constituent elements
    material = path.split('/')[-5]
    elements = re.findall('[A-Z][a-z]|[A-Z]', material)

    #Get energies, DOS and integrated DOS
    energies = dos['energy'] 
    total_dos = dos['total_dos']
    integrated_dos = dos['integrated_dos']

    #Get reference
    if reference == 'fermi_level':
        ref = Ef
    elif reference == 'vacuum_level':
        _, __, evac = get_averaged_hartree_potential_tube(path) #get vacuum level from calculation in k_mesh folder
        ref = evac

    if plot_total_dos:
        E, DOS = fold_dos(energies-ref, total_dos, npoints, width)
        plt.plot(E, DOS, 'k', label = 'Total DOS')

    #Plot pdos
    for i, pd in enumerate(pdos_to_plot):
        E, PDOS = fold_dos(energies-ref, dos[pd], npoints, width)
        plt.plot(E, PDOS + i*shift, label = pd)

    #Plot band edges
    if state == 'semiconductor':
        plt.axvline(Ev-ref, label = 'Valence band edge', color = 'black', linestyle = '--', linewidth = 1)
        plt.axvline(Ec-ref, label = 'Conduction band edge', color = 'black', linestyle = '--', linewidth = 1)
    elif state == 'metal':
        plt.axvline(Ef-ref, label = 'Fermi level', color = 'black', linestyle = '--', linewidth = 1)

    emin = (Ef - ref) + emin
    emax = (Ef - ref) + emax
    plt.xlabel('Energy [eV]')
    plt.ylabel('Density of state [a.u.]')
    plt.xlim(emin, emax)
    plt.ylim(dos_min, dos_max)
    #plt.legend()

    if show:
        plt.show()



def plot_averaged_hartree_potential_tube(path, plot_band_edges = True, show = True):


    #Get averaged hartree potential
    r, v, evac = get_averaged_hartree_potential_tube(path)

    #Get band edge parameters
    Ec, Ev, Ef, state = get_band_edge_parameters(path)

    #Make plot
    plt.plot(r,v)

    if state == 'semiconductor':
        plt.plot(r, Ec*np.ones(len(r)), 'k')
        plt.plot(r, Ev*np.ones(len(r)), 'k', label = 'Band edges')
    if state == 'metal':
        plt.plot(r, Ef*np.ones(len(r)), 'k--', label = 'Fermi level')

    plt.xlabel('Distance from tube center [$\AA$]')
    plt.ylabel('Averaged potential [eV]')
    plt.plot(r,v)
    plt.legend()
    if show:
        plt.show()



def plot_averaged_hartree_potential_sheet(path, plot_band_edges = True, show = True):


    #Get averaged hartree potential
    r, v, evac1, evac2 = get_averaged_hartree_potential_sheet(path)

    #Print vacuum levels 
    print('Vacuum level below sheet: {}eV.'.format(evac1))
    print('Vacuum level above sheet: {}eV.'.format(evac2))

    #Get band edge parameters
    Ec, Ev, Ef, state = get_band_edge_parameters(path)

    #Make plot
    plt.plot(r,v)

    if state == 'semiconductor':
        plt.plot(r, Ec*np.ones(len(r)), 'k')
        plt.plot(r, Ev*np.ones(len(r)), 'k', label = 'Band edges')
    if state == 'metal':
        plt.plot(r, Ef*np.ones(len(r)), 'k--', label = 'Fermi level')


    plt.xlabel('z [$\AA$]')
    plt.ylabel('Averaged potential [eV]')
    plt.plot(r,v)
    plt.legend()
    if show:
        plt.show()




#band structure plots
#path = '/home/energy/aegm/Nanotubes_v2/MoSTe_v2/MoS2/MoSTe/armchair/24_0/nm/bandstructure'
#emin, emax = -3, 3
#plot_bandstructure(path, emin, emax, reference = 'vacuum_level', plot_tube = True, plot_ribbon = True, plot_tube_Fermi_level = True, show = True)


#band gap plots
#plot_bandgap_vs_diameter(path = '/home/energy/aegm/Nanotubes_v2/MoSTe_v2/MoS2/MoSTe/armchair/')



#dos NOTE: Aligning pdos for 2D sheet to vacuum level will not work! 
#pdos_to_plot = ['s_Mo', 'p_Mo', 'd_Mo',  's_S', 'p_S', 'd_S', 's_Te', 'p_Te', 'd_Te']
#path = '/home/energy/aegm/Nanotubes_v2/MoSTe_v2/MoS2/MoSTe/armchair/24_0/nm/fine_k_mesh'
#path = '/home/energy/aegm/Nanotubes_v2/MoSTe_v2/MoS2/MoSTe/2DSheet/fine_k_mesh'
#shift = 25, dox_max = 250 for stacked plots
#plot_dos(path, pdos_to_plot = pdos_to_plot, emin = -3, emax = 3, dos_min = 0, dos_max = 30, width = 0.1, npoints = 5000, shift = 0, reference = 'vacuum_level', normalize = True, plot_total_dos = False, plot_band_edges = True, show = True)





#tube potential
#path = '/home/energy/aegm/Nanotubes_v2/MoSTe/MoS2/MoSTe/armchair/12_0/nm/fine_k_mesh'
#plot_averaged_hartree_potential_tube(path)

#sheet potential
#path = '/home/energy/aegm/Nanotubes_v2/MoSTe/MoS2/MoSTe/2DSheet/fine_k_mesh'
#plot_averaged_hartree_potential_sheet(path)





