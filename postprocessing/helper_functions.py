from ase.calculators.vasp import Vasp, Vasp2, VaspDos, VaspChargeDensity
from ase.dft.bandgap import bandgap
from pathlib import Path
import numpy as np
import os
from ase.io import *
from scipy import interpolate
from math import sqrt

def get_band_edge_parameters(path):

    calc = Vasp2(restart = True, directory = path)
    nocc = int(calc.get_number_of_electrons()/2)
    bs = calc.band_structure()
    energies = bs.energies
    Ev = energies[0,:,nocc-1].max() #valence band edge
    Ec = energies[0,:,nocc].min() #conduction band edge

    if Ec > Ev: #semiconductor
        Ef = (Ev + Ec)/2 #In this case place Fermi level in the middle of the gap
        state = 'semiconductor'
    else:
        Ef = calc.get_fermi_level()
        state = 'metal' #In this case we trust the Vasp Fermi level
        Ec = None
        Ev = None

    return Ev, Ec, Ef, state

def get_work_function_tube(path):

    _, __, Ef, ___ = get_band_edge_parameters(path)
    _, __, evac = get_averaged_hartree_potential_tube(path)

    work_function = evac - Ef

    return work_function


def get_work_function_sheet(path):

    _, __, Ef, ___ = get_band_edge_parameters(path)
    _, __, evac1, evac2 = get_averaged_hartree_potential_sheet(path)

    work_function1 = evac1 - Ef
    work_function2 = evac2 - Ef

    return work_function1, work_function2


def get_averaged_hartree_potential_tube(path):


    #Read atoms object
    atoms = read(path + '/OUTCAR')
    
    #Interpolate real-space grid
    dr = 0.1 #Thickness for circular averaging. Potential is averaged in a circular shell of thickness dr.
    interpolation_factor = 10 #All grids will be interpolated to have an interpolation factor more points to get better smoothing. 

    #Get potential and average in z.
    A = read(path + '/POSCAR')
    pot = VaspChargeDensity(filename = path + '/LOCPOT')
    v = pot.chg[0]*A.get_volume()
    v_xy_old = v.mean(axis=2)

    #Setup grids 
    nx, ny, nz = v.shape #number of points in x y and z direction
    Lx = atoms.get_cell()[0,0]
    Ly = atoms.get_cell()[1,1]
    x_old = np.linspace(-Lx/2, Lx/2, nx, endpoint=False)
    y_old = np.linspace(-Ly/2, Ly/2, ny, endpoint=False)

    #Interpolate grids and potential for better resolution
    f = interpolate.interp2d(x_old,y_old,v_xy_old,kind='cubic')
    x = np.linspace(-Lx/2, Lx/2, interpolation_factor*nx, endpoint = False)
    y = np.linspace(-Ly/2, Ly/2, interpolation_factor*ny, endpoint = False)
    v_xy = f(x,y)
    nx, ny = v_xy.shape
    vr = v_xy.flatten()
    distances = np.linalg.norm(np.array(np.meshgrid(x,y)).T.reshape(-1,2), axis = 1)
    r_max = max(distances)
    vr_averaged = []
    r_averaged = []


    r = 0
    while r<r_max+dr/2:
        temp1 = distances[np.where(np.logical_and(distances >= r-dr/2, distances < r+dr/2))]
        temp2 = vr[np.where(np.logical_and(distances >= r-dr/2, distances < r+dr/2))]
        r_averaged.append(np.mean(temp1))
        vr_averaged.append(np.mean(temp2))
        r = r+dr


    evac = vr_averaged[-1] #Get vacuum level. 
    
    return r_averaged, vr_averaged, evac


def get_averaged_hartree_potential_sheet(path):

    #Read atoms object
    atoms = read(path + '/OUTCAR')

    #Get potential and average in x, y.
    A = read(path + '/POSCAR')
    pot = VaspChargeDensity(filename = path + '/LOCPOT')
    v = pot.chg[0]*A.get_volume()
    vz = np.mean(np.mean(v, axis = 0), axis = 0)

    #Setup grids 
    nz = len(vz) #number of points in z direction
    Lz = atoms.get_cell()[2,2]
    z = np.linspace(0, Lz, nz, endpoint = False)

    #Vacuum levels
    evac1, evac2 = vz[20], vz[-1]

    return z, vz, evac1, evac2



def get_bandgap(path, direct = False):

    calc = Vasp2(restart = True, directory = path)
    gap, p1, p2 = bandgap(calc, direct = direct)

    return gap


def get_dos(path, normalize = True):

    atoms = read(path + '/OUTCAR')
    orbitals = ['s', 'px', 'py', 'pz', 'dxy', 'dxz', 'dyz', 'dx2','dz2']
    DOS = VaspDos(doscar = path + '/DOSCAR')
    energy = DOS.energy
    total_dos = DOS.dos
    integrated_dos = DOS.integrated_dos
    dos = {}
    dos['energy'] = energy

    if normalize:
        dos['total_dos'] = total_dos/len(atoms)
        dos['integrated_dos'] = integrated_dos/len(atoms)
    else:
        dos['total_dos'] = total_dos
        dos['integrated_dos'] = integrated_dos
        
    dos['orbitals'] = orbitals
    elements = set(atoms.get_chemical_symbols())

    for orbital in orbitals:
        for element in elements:
            
            pdos = 0            
            for atom in atoms:
                if atom.symbol == element:
                    pdos = pdos + DOS.site_dos(atom.index, orbital)
            
            if normalize:
                N_element = len([symbol for symbol in atoms.get_chemical_symbols() if symbol == element])
                pdos = pdos/N_element
            
            dos[orbital + '_' + element] = pdos

    for element in elements:
        dos['p_{}'.format(element)] = dos['px_{}'.format(element)] + dos['py_{}'.format(element)] + dos['pz_{}'.format(element)] 
        dos['d_{}'.format(element)] = dos['dxy_{}'.format(element)] + dos['dxz_{}'.format(element)] + dos['dyz_{}'.format(element)] + dos['dx2_{}'.format(element)] + dos['dz2_{}'.format(element)] 

    return dos



def get_tube_bandstructure(path):
    
    #Start by checking that calculation has been carried out
    if not Path(path + '/nts.bandstructure_vasp.done').is_file():
        raise FileNotFoundError('Band structure calculation has not finished.')

    #Now retrieve band structure
    calc = Vasp2(restart = True, directory = path)
    bandstructure = calc.band_structure()
    bs = bandstructure.todict()
    kpoints = bs['kpts'][:,-1]
    energies = bs['energies']
    nbands = len(energies[0,2,:])

    return kpoints, energies, nbands



def get_ribbon_bandstructure(path):
    
    #First check that calculation has been carried out
    if not Path(path + '/nts.ribbon_vasp.done').is_file():
        raise FileNotFoundError('Ribbon calculation has not finished.')

    #Retrieve information about ribbon from folder structure
    properties = path.split('/')
    kind = properties[-4]
    nunitcells = int(properties[-3].split('_')[0])
    npoints = 48 #This should be consistent with what is used in ribbon_vasp.py


    #Then load band structure object
    calc = Vasp2(restart = True, directory = path)
    bandstructure = calc.band_structure()
    bs = bandstructure.todict()
    kpoints = bs['kpts']
    energies = bs['energies'] 

    #Now do some restructuring of kpoints and band structure energies
    nbands = nunitcells * len(energies[0,0,:])
    if kind == 'armchair':
        kpoints = kpoints[:,0]
    if kind == 'zigzag':
        kpoints = kpoints[0,:]

    kpoints_reduced = []
    energies_reduced = np.empty([npoints,nbands])
        
    for i in range(npoints):
        kpoints_reduced = np.append(kpoints_reduced,kpoints[i])
        indices = kpoints==kpoints[i]
        energies_same_k = energies[0,indices,:]
        energies_reduced[i,:] = energies_same_k.flatten()

    return kpoints_reduced, energies_reduced, nbands



def delta(x, x0, width, mode='Gauss'):
    """Return a gaussian of given width centered at x0."""
    pi = np.pi
    if mode == 'Gauss':
        return np.exp(np.clip(-((x - x0) / width)**2,
                              -100.0, 100.0)) / (np.sqrt(pi) * width)
    if mode == 'Lorentz':
        return (2 / pi / width) / ((np.clip(((x - x0) / (width / 2))**2,
                                            -100.0, 100.0)) + 1)


def fold_dos(energies, dos, npts, width, mode='Gauss'):
    """Add gaussian smearing to DOS point - function borrowed from GPAW.
    width=0 disables gaussian smearing. """
    if width == 0:
        return energies, dos
    emin = min(energies) - 5 * width
    emax = max(energies) + 5 * width
    e = np.linspace(emin, emax, npts)
    dos_e = np.zeros(npts)

    for e0, w in zip(energies, dos):
        dos_e += w * delta(e, e0, width, mode=mode)
    return e, dos_e


    
    
