# File for VASP specific functions that make life easier
from ase.io import write,iread,Trajectory
import numpy as np
from ase.calculators.vasp import Vasp2

def get_total_elastic_tensor_vasp(outcar_path):
    '''returns total elastic tensor C from VASP OUTCAR file'''
    elastic = list()
    append = False
    counter = 0
    with open(outcar_path,'r') as f:
        for line in f:
            if line.rfind('TOTAL ELASTIC MODULI')> -1:
                append = True
            if append:
                if counter > 2:
                    elastic.append([float(x)/10 for x in line.split()[1:]]) # convert from (kbar) to (GPa)
                counter += 1
                if counter == 9:
                    return np.array(elastic)

def get_runtime_vasp(outcar_path):
    '''returns the Elapsed time of the job on the cluster -> time that it took
    until results are actually available.
    returns vasp time in s'''
    with open(outcar_path,'r') as f:
        for line in f:
            if line.rfind('Elapsed time')> -1:
                return float(line.split()[-1])

def get_number_ionic_steps_vasp(outcar_path):
    '''Reads last iteration step number from OUTCAR file'''
    iterations = list()
    with open(outcar_path,'r') as f:
        for line in f:
            if line.rfind('Iteration')> -1:
                iterations.append(line)
    # then only return last iteration number stored in OUTCAR file
    return int(iterations[-1].split()[2].split('(')[0])
    
def OUTCAR_to_traj(outcar_path, name = 'ase'):
    '''Writes ase.traj file from OUTCAR file'''
    write('{}.traj'.format(name), iread(outcar_path))
    return True

def get_parameters():
    '''Choose recommended (not default) VASP  pseudo-potentials
    
    https://cms.mpi.univie.ac.at/vasp/vasp/Recommended_PAW_potentials_DFT_calculations_using_vasp_5_2.html'''
    parameters = {'setups':{'H':'',
                            'He':'',
                            'Li':'_sv',
                            'Be':'',
                            'B':'',
                            'C':'',
                            'N':'',
                            'O':'',
                            'F':'',
                            'Ne':'',
                            'Na':'_pv',
                            'Mg':'',
                            'Al':'',
                            'Si':'',
                            'P':'',
                            'S':'',
                            'Cl':'',
                            'Ar':'',
                            'K':'_sv',
                            'Ca':'_sv',
                            'Sc':'_sv',
                            'Ti':'_sv',
                            'V':'_sv',
                            'Cr':'_pv',
                            'Mn':'_pv',
                            'Fe':'',
                            'Co':'',
                            'Ni':'',
                            'Cu':'',
                            'Zn':'',
                            'Ga':'_d',
                            'Ge':'_d',
                            'As':'',
                            'Se':'',
                            'Br':'',
                            'Kr':'',
                            'Rb':'_sv',
                            'Sr':'_sv',
                            'Y':'_sv',
                            'Zr':'_sv',
                            'Nb':'_sv',
                            'Mo':'_sv',
                            'Tc':'_pv',
                            'Ru':'_pv',
                            'Rh':'_pv',
                            'Pd':'',
                            'Ag':'',
                            'Cd':'',
                            'In':'_d',
                            'Sn':'_d',
                            'Sb':'',
                            'Te':'',
                            'I':'',
                            'Xe':'',
                            'Cs':'_sv',
                            'Ba':'_sv',
                            'La':'',
                            'Ce':'',
                            'Pr':'_3',
                            'Nd':'_3',
                            'Pm':'_3',
                            'Sm':'_3',
                            'Eu':'_2',
                            'Gd':'_3',
                            'Tb':'_3',
                            'Dy':'_3',
                            'Ho':'_3',
                            'Er':'_3',
                            'Tm':'_3',
                            'Yb':'_2',
                            'Lu':'_3',
                            'Hf':'_pv',
                            'Ta':'_pv',
                            'W':'_pv',
                            'Re':'',
                            'Os':'',
                            'Ir':'',
                            'Pt':'',
                            'Au':'',
                            'Hg':'',
                            'Tl':'_d',
                            'Pb':'_d',
                            'Bi':'_d',
                            'Po':'_d',
                            'At':'_d',
                            'Rn':'',
                            'Fr':'_sv',
                            'Ra':'_sv',
                            'Ac':'',
                            'Th':'',
                            'Pa':'',
                            'U':'',
                            'Np':'',
                            'Pu':'',
                            'Am':'',
                            'Cm':''}}
    return parameters



def get_number_of_electrons(path):
    '''Sets the number of bands empty bands to converge by reading the number of valence electrons in the system from the previous relaxation.'''

    calc_old = Vasp2(restart = True, directory = path)
    n_electrons = calc_old.get_number_of_electrons()
  
    return n_electrons



def get_ribbon_kpts(kind, npoints, nunitcells):

    kpts = []
    dk = 0.5/(npoints-1) #kpoint spacing

    if kind == 'armchair':
        for i in range(nunitcells):
            for j in range(npoints):
                kpts.append([j*dk, i/nunitcells,0])    
    elif kind =='zigzag':
        for i in range(nunitcells):
            for j in range(npoints):
                kpts.append([i/nunitcells,j*dk,0])
    return kpts





def get_ribbon_kpts_v2(kind, npoints, nunitcells):

    if kind == 'armchair':


        dk = 0.5/(npoints-1) #kpoint spacing for armchair
        kpoints = []

        #Start with Gamma-M
        for i in range(nunitcells):
            for j in range(npoints):
                kpoints.append([j*dk,0,0])    

        Gamma_M = np.array(kpoints)

        kpoints = []

        #Next Gamma-K
        for i in range(nunitcells):
            for j in range(npoints):
                kpoints.append([i/nunitcells, i/nunitcells,0])    

        Gamma_K = np.array(kpoints)
    
        kpts = Gamma_M + Gamma_K


    if kind == 'zigzag':

        kpoints = []
        dk = 0.33333/(npoints-1) #kpoint spacing for armchair


        #Start with Gamma-K
        for i in range(nunitcells):
            for j in range(npoints):
                kpoints.append([j*dk,j*dk,0])    

        Gamma_K = np.array(kpoints)

        kpoints = []

        #Next Gamma-M
        for i in range(nunitcells):
            for j in range(npoints):
                kpoints.append([i/nunitcells, 0, 0])    

        Gamma_M = np.array(kpoints)
    
        kpts = Gamma_M + Gamma_K



    return kpts
