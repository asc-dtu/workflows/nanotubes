'''
SCRIPT for bulk relaxations

references from MATERIALSPROJECT database

www.materialsproject.org

Database file has been created using 'queuery_data_from_MPDatabase.py' script
script can be found here: https://gitlab.com/ivca/Nanotubes/tree/master/additional
author: Felix Bölle - feltbo@dtu.dk
'''

from contextlib import contextmanager
import os,sys
#import matplotlib.pyplot as plt
import re
import numpy as np
from collections import Counter

from ase.db import connect
from ase.calculators.vasp import Vasp

from nts.system_utils import cd

# refrence databases
db_ref = connect('/home/energy/ivca/Nanotubes_project/MPReference.db')
db = connect('bulk_reference_states.db') # make it in your home folder and merge with /home/energy/ivca/Nanotubes_project/bulk_reference_states.db database after

def get_custom_kpts(cell):
    ''' return kpoints based on criterion
    30/a = min number of kpoints '''
    k_crit = 30 # which kpoint density 30/a = min kpoints
    t1,t2,t3 = cell[0,:],cell[1,:],cell[2,:]

    kpts = [int(k_crit/np.linalg.norm(t1)) + 1,
            int(k_crit/np.linalg.norm(t2)) + 1,
            int(k_crit/np.linalg.norm(t3)) + 1]

    return kpts

def material_decompose(material):
    '''returns elements from material string
    just like set(atoms.get_chemical_symbols())'''
    mat_el = re.findall('[A-Z][^A-Z]*', material)
    mat_sym = []
    for el in mat_el:
        mat_sym.append(''.join([i for i in el if not i.isdigit()]))
        
    return mat_sym

def needed_relaxations(materials):
    to_relax = []
    for material in materials:
        print(material)
        mat_dec = material_decompose(material)
        for row in db_ref.select():
            if row.u == True: # take out the DFT + U relaxations
                continue
            if row.formula =='O8': # want my own O-water reference
                continue
            try:
                db.get(formula=row.formula) # relaxed already
                continue
            except KeyError:
                mat_db = material_decompose(row.formula)
                if all([el in mat_dec for el in mat_db]):
                    to_relax.append(row.formula);print(row.formula);print(to_relax)
    return len(set(to_relax))

def check_energies_available(material):
    '''script looks in the db_ref database for all bulk compositions that can be
    made out of the material, e.g. MoS2 will look for all material in the database
    that have Mo or S (or both) in it: Mo,S,Mo2S4 in this case. If it is not in
    the db (bulk reference states) those structures will be relaxed.'''
    # VASP specific
    isif = 3 # bulk relaxation
    ibrion = 2
    ecut = 550

    mat_dec = material_decompose(material)
    to_relax = []
    for row in db_ref.select():
        if row.formula =='O8': # want my own O-water reference
            continue
        mat_db = material_decompose(row.formula)
        if all([el in mat_dec for el in mat_db]):
            to_relax.append([row.id,row.formula])
    
    for row_id,formula in to_relax:
        id = db.reserve(name=formula)
        if id is None:
            continue
        
        atoms_rel = db_ref.get_atoms(id=row_id)
        kpoints = get_custom_kpts(atoms_rel.get_cell()) # make sure that certain kpoint density specified is fulfilled 
        ediffg = -0.02
        calc_vasp = Vasp(xc='pbe',
                             encut=ecut,
                             ismear=-5, # as in materialsproject and recommended VASP
                             sigma=0.1,
                             isif = isif,
                             ncore = 24,
                             kpts=kpoints,
                             nsw = 200,
                             algo='normal',
                             ediff = 0.00001,
                             ediffg = ediffg,
                             nelmin  = 8, # Juanma recommended
                             ibrion = ibrion,
                             ispin = 2, # spin polarized
                             gamma = True,
                             lwave = True, 
                             lcharg = False,
                             prec = 'accurate',
                             setups = 'recommended')
        atoms_rel.set_calculator(calc_vasp)

        # set magnetic moment
        magmoms_ref = db_ref.get(id=row_id).data['magmoms_per_site']
        if len(magmoms_ref) == 0:
            # no reference value, set to 1 for all
            atoms_rel.set_initial_magnetic_moments(np.array([1 for i in range(len(atoms_rel))]))
        else:
            atoms_rel.set_initial_magnetic_moments(magmoms_ref)
            
        with cd('bulk_structures'):
            with cd(formula):
                e = atoms_rel.get_potential_energy()

                # check for force convergence
                fmax = atoms_rel.get_forces().max()
                if fmax > abs(ediffg):
                    raise ValueError('Forces not converged.')

                elements = ''.join([i for i in material_decompose(formula)])
                db.write(atoms_rel,name=formula,elements=elements,
                         energy_per_atom = e/len(atoms_rel))
                # remove WAVECAR when finished
                os.remove('WAVECAR')
        del db[id]
        sys.exit()
        
if __name__=='__main__':
    metals = ['Ti','Ge','Mo','Zr','W','Ta','Nb','Sn','V','Fe','Hf']
    groups = ['OS','OSe','OTe','STe','SSe']
    materials = [metal + group for metal in metals for group in groups]
    metals_augfel = ['Bi','Sb','As']
    groups_felix = ['SCl','SBr','SI','SeCl','SeBr','SeI','TeCl','TeBr','TeI']
    groups_august = ['ClS','ClSe','ClTe','BrS','BrSe','BrTe','IS','ISe','ITe']
    metals_ivano = ['Zr','Hf','Mo','Nb','Ta','Ti','V','W']
    groups_ivano = ['ClBr','ClI','BrI']
    materials = materials + \
            [metal + group for metal in metals_augfel for group in groups_august] + \
            [metal + group for metal in metals_augfel for group in groups_felix] + \
            [metal + group for metal in metals_ivano for group in groups_ivano]
    if 0: # first check how often script needs to be submitted
        n_rel = needed_relaxations(materials)
        print('Relaxations needed:', n_rel)  # takes some time
    if 1:
        # submit with something like this in shell:
        # for i in $(seq n_rel);do mq submit relax_bulk_vasp.py@24x12h;done
        for material in materials:
            check_energies_available(material)
        
    
