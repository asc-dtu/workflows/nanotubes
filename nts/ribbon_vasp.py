#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Thu Mar 14 14:05:55 2019

Script for doing fine k-mesh calculations for 2D sheets.

@author: August Edwards <aegm@dtu.dk>
'''

from ase.io import read,write

from nts.vasp_relaxation_setups import get_vasp_calc_initial_relaxation_sheet, setup_calc_dipole_sheet, get_custom_kpts
from nts.sheet_tools import hexagonal_cell_to_orthorhombic
from nts.vasp_helper import get_number_of_electrons, get_ribbon_kpts
from pathlib import Path
from ase.calculators.vasp import Vasp, Vasp2
import numpy as np
import os


def ribbon():

    #--------------------- STEP 1: retrieve Structure -------------------------


    # get nanotube kind (armchair/zigzag) and  number of 2D unit cell repitions from folder structure
    cwd = os.getcwd()
    properties = cwd.split('/')
    kind = properties[-4]
    nunitcells = int(properties[-3].split('_')[0])

    #Then read 2D atoms object from relaxation
    atoms_2d = read('../../../../2DSheet/vasp_rx/OUTCAR')

    #Finally convert to orthorhomic cell
    atoms_2d = hexagonal_cell_to_orthorhombic(atoms_2d)



    #-------------------------- STEP 2: SCF calculation which saves CHGCAR ----------------------
    calc = get_vasp_calc_initial_relaxation_sheet(atoms_2d)
    calc = setup_calc_dipole_sheet(atoms_2d, calc)
    n_electrons = get_number_of_electrons(path = '../../../../2DSheet/vasp_rx') #deduce number of electron from previous relaxation
    nbands = n_electrons//2 + 100 #100 empty bands
    kpts = get_custom_kpts(atoms_2d.get_cell(), density_criterion = 100)
    kpts = [kpts[0], kpts[1], 1]
    calc.set(nbands = nbands)
    calc.set(ibrion=-1)
    calc.set(nsw=0)
    calc.set(kpts = kpts)
    calc.set(lvhar = True)
    calc.set(lwave = False)
    calc.set(lcharg = True)
    atoms_2d.set_calculator(calc)
    atoms_2d.get_potential_energy()


    #-------------------------- STEP 3: Non SCF calculation with specially made k-mesh ----------------------
    npoints = 48 #Number of kpoints in z direction 
    calc.set(icharg = 11)
    kpts = get_ribbon_kpts(kind = kind, npoints = npoints, nunitcells = nunitcells)
    calc.set(kpts = kpts)
    calc.set(isym = 0)
    calc.set(reciprocal = True)
    calc.set(kpts_nintersections = npoints)
    atoms_2d.set_calculator(calc)
    atoms_2d.get_potential_energy()


if __name__=='__main__':
    state = ribbon()
