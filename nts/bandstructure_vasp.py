#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Thu Mar 14 14:05:55 2019

Script for doing fine k-mesh calculations for 2D sheets.

@author: August Edwards <aegm@dtu.dk>
'''


from ase.io import read,write

from nts.vasp_relaxation_setups import get_vasp_calc_initial_relaxation_tube, get_custom_kpts
from nts.vasp_helper import get_number_of_electrons
from pathlib import Path
from ase.calculators.vasp import Vasp, Vasp2
import numpy as np
from shutil import copy


def bandstructure():

    #--------------------- STEP 1: retrieve Structure -------------------------
    #Then read atoms object from relaxation
    atoms = read('../vasp_rx/OUTCAR')


    #-------------------------- STEP 2: SCF calculation which saves CHGCAR ----------------------
    calc = get_vasp_calc_initial_relaxation_tube(atoms)
    kpts = get_custom_kpts(atoms.get_cell(), density_criterion = 100)
    kpts = [1, 1, kpts[2]] # vacuum along z-axis
    n_electrons = get_number_of_electrons(path = '../vasp_rx')
    nbands = n_electrons//2 + 100 #100 empty bands 
    calc.set(nsw = 0)
    calc.set(kpts = kpts)
    calc.set(lwave = False)
    calc.set(lcharg = True)
    calc.set(nbands = nbands)
    atoms.set_calculator(calc)
    atoms.get_potential_energy()


    #-------------------------- STEP 3: Single point calculation with fixed density ----------------------
    calc = Vasp2(restart = True)
    kpts = {'path': 'GX', 'npoints': 40}  
    calc.set(icharg = 11)
    calc.set(kpts = kpts)
    calc.set(isym = 0)
    atoms.set_calculator(calc)
    atoms.get_potential_energy()
    


if __name__=='__main__':
    state = bandstructure()
