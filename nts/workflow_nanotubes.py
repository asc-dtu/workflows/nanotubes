from myqueue.task import task
import os
import pickle
from nts.system_utils import cd

def get_wd(folders):
    ''' Function creates the folder structure for the calculation
    according to cwd + folders
    folders as list: e.g. folders = ['1','2','3'] creates cwd/1/2/3'''
    cwd = os.getcwd()
    for folder in folders:
        with cd(os.path.join(cwd,folder)):
            cwd = os.getcwd()
            continue
    return cwd

# run multiple tubes
multiple = True # set True if multiple nanotubes
if multiple:
    with open('current_tube','rb') as f:
        user_args = pickle.load(f)
else:
    # run a specific tube
    user_args = ['MoS2','MoS2','armchair','6_0','nm']
    
# input for run
prototype, material, kind, n_c, magstate = user_args
n,c = int(n_c.split('_')[0]),int(n_c.split('_')[1])


# default folder structures
default = [prototype,material,kind,n_c,magstate]
def_elec = default + ['electronic_properties']
def_stab = default + ['stability_properties']

# get the needed folders
vasp_rx_folder = get_wd(default + ['vasp_rx'])
electronic_folder = get_wd(def_elec)
bandgap_folder = get_wd(def_elec + ['bandgap'])
bandstructure_folder = get_wd(def_elec + ['bandstructure'])
dos_folder = get_wd(def_elec + ['dos'])
ribbon_folder = get_wd(def_elec + ['ribbon'])
band_edge_folder = get_wd(def_elec + ['band_edge_parameters'])
potential_folder = get_wd(def_elec + ['potential'])
stripe_folder = get_wd(def_stab + ['stripe'])


def create_tasks():
    j1 = task('nts.relax_tube_vasp@80:xeon40:80:2d', folder = vasp_rx_folder)
    j2 = task('nts.electronic_vasp@40:xeon40:40:2d', folder = electronic_folder, deps = [j1])
    #j3 = task('nts.ribbon_vasp@8x1d', folder = ribbon_folder)
    #j4 = task('nts.stripe_vasp@24x1d', folder = stripe_folder)
    #tasks = [j1,j2,j3]
    #if kind == 'armchair' and n <= 10: # include around three stripes for extrapolation scheme ~1/d
    #    tasks += [j4]
    #if kind == 'zigzag' and n <= 14:
    #    tasks += [j4]
    #tasks = [j1,j2]
    return [j1]
