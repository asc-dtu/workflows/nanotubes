#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Thu Mar 14 11:15:55 2019

Script for relaxing nanotubes using VASP and ASE

@author: Felix Bölle <feltbo@dtu.dk>
'''
### standard libraries
import os

### ASE
from ase.io import read

### specific functions for nanotube workflow
from nts.nanotube_tools import (create_janus_MXY_tube_from_MX2_tube,wrap_2D,
                            decomposed_material_string)
from nts.sheet_tools import check_correct_XY_order,hexagonal_cell_to_orthorhombic
from nts.structure_input import get_2D_structure_from_cmr2Ddb,create_janus_from_cmr2Ddb
from nts.vasp_relaxation_setups import (get_vasp_calc_initial_relaxation_tube,
                                    initial_relaxation_routine_vasp_tube)

def relax_tube_vasp():
    #--------------------- STEP 1: retrieve Structure -------------------------
    
    # get information about calculation from folder name
    # also ensures that this is correct folder to carry out calculation
    cwd = os.getcwd()
    properties = cwd.split('/')
    prototype = properties[-6]
    material = properties[-5]
    kind = properties[-4]
    n,c = int(properties[-3].split('_')[0]),int(properties[-3].split('_')[1])
    magstate = properties[-2]
    
    

    # now get a tube - either wrap a new one or take a prototype tube
    element_list = decomposed_material_string(material)
    if len(element_list) > 2: # everything for now with more than 2 elements, that is a YMX structure
        el_m,el_i,el_o = element_list
        # look if that structure is availabe in the c_2db database first before creating it on your own
        try:
            atoms_2d = get_2D_structure_from_cmr2Ddb(prototype,material)
            atoms_2d = check_correct_XY_order(atoms_2d,el_i,el_o)
            atoms_2d = hexagonal_cell_to_orthorhombic(atoms_2d)
            atoms = wrap_2D(atoms_2d,material,kind,n,c)
        except: # that is the creating it on your own part - work in progress !!!
            formula = ''.join([el_m,el_i,'2']) # formula = M + el_insidey
            #Get 2D atoms from Kristians database
            atoms_2d = get_2D_structure_from_cmr2Ddb(prototype,formula)
            atoms_2d = hexagonal_cell_to_orthorhombic(atoms_2d)
            # get guess from averaged lattice for janus structure
            atoms_2d = create_janus_from_cmr2Ddb(el_m,el_i,el_o,prototype,atoms_2d)
            atoms = wrap_2D(atoms_2d,material,kind,n,c)
            # robust if just wrapped from prototype, but how much slower?
            atoms = create_janus_MXY_tube_from_MX2_tube(atoms,el_m,el_i,el_o)
    ### in case one does not want to run a janus nanotube
    else: # new prototype needs to be wrapped from 2D-database
        #Get 2D atoms from Kristians database
        atoms_2d = get_2D_structure_from_cmr2Ddb(prototype,material)
        if prototype in ['MoS2', 'CdI2', 'BN', 'C', 'MoSSe','BiTeI']:
            atoms_2d = hexagonal_cell_to_orthorhombic(atoms_2d)
        atoms = wrap_2D(atoms_2d,material,kind,n,c)
    
    # insert vacuum and center atoms
    atoms.center(vacuum=8,axis=[0,1])
    atoms.set_pbc = ([True, True, True])
    
    #-------------------------- STEP 2: SETUP RELAXATION ----------------------
    calc_vasp = get_vasp_calc_initial_relaxation_tube(atoms)
    
    
    #------------------------- STEP 3: RELAX STRUCTURE ------------------------
    status = initial_relaxation_routine_vasp_tube(atoms,calc_vasp)
    
    return status

if __name__=='__main__':
    state = relax_tube_vasp()
    if state == 'nsteps':
        raise ValueError('Maximum number of ionic steps reached.')
    elif state == 'circular':
        raise ValueError('Relaxed tube not circular.')
    elif state == 'forces':
        raise ValueError('Forces not converged.')
    elif state == 'counter':
        raise ValueError('Maximum number of restarts (10) reached.')


