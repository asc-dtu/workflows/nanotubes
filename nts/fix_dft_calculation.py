#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 30 15:31:38 2019

Script for handling common errors occuring during DFT calculations. Inspired
by Custodian as well as the many hours spended on trying to converge failed DFT
calculations.

@author: Felix Bölle <feltbo@dtu.dk>
"""

import os
import logging
from pathlib import Path
from shutil import copy2
import json

from ase.io import read, write,Trajectory

_vasp_out_errors = {'notHermitian': 'WARNING: Sub-Space-Matrix is not hermitian in DAV',
                    'invgrp': 'inverse of rotation matrix was not found (increase SYMPREC)',
                    'rhosyg': 'RHOSYG internal error: stars are not distinct',
                    'zbrent': 'ZBRENT: fatal error',
                    'posmap': 'POSMAP internal error: symmetry equivalent atom not found',
                    'fexcp' : 'ERROR FEXCP: supplied Exchange',
                    'pzstein': 'PZSTEIN parameter number',
                    'sgrcon': 'internal error in subroutine SGRCON',
                    'tetrahedron': 'Tetrahedron method fails for NKPT<4',
                    'brmix': 'BRMIX: very serious problems'
                    }

_vasp_error_adjustments = {'notHermitian': {'_set': {'prec':'accurate'}}, # The PREC-flag determines the energy cutoff ENCUT, if (and only if) no value is given for ENCUT in the INCAR file.  (VASP-wiki)
                           'invgrp':{'_set':{'isym':0}},
                           'rhosyg':{'_set':{'symprec':1e-4}},
                           'zbrent_old':{'_set':{'ibrion':1},'_copy_file':{'ase-sort.dat', 'ase-sort_0.dat'},'_atoms':{'CONTCAR'}}, # copy CONTCAR to POSCAR (by setting atoms to CONTCAR) and set ibrion to 1
                           'zbrent':{'_set':{'algo':'all','ediff':1e-7}},
                           'posmap':{'_set':{'symprec':1e-8}},
                           'fexcp' :{'_set':{'algo':'all'}},
                           'pzstein':{'_set':{'algo':'all'}},
                           'sgrcon':{'_set':{'isym':0, 'ediff':1e-7}}, # isym=0 might already lead to convergence, ediff just added for robustness
                           'tetrahedron':{'_set':{'ismear':0}},
                           'brmix': {'_set':{'algo':'all', 'isym':0, 'ediff':1e-7}}
                           }

# A list of errors that occur in vasp.out at the same time
# first the combination is given, second which adjustment to choose in this case
# e.g. {'brmix': {'zbrent': 'zbrent'}}. brmix error occurs together with 
# zbrent error -  choose zbrent error adjustemnt
_known_error_combinations = {'brmix': {'zbrent': 'zbrent'}}

class ErrorStrings:
    """ Strings used to check status of job by scanning files """
    time_out = 'DUE TO TIME LIMIT'
    out_of_memory = 'task 0: Out Of Memory'
    
class FixVaspCalculation:
    """ Class for handling common VASP errors or convergence issues """
    

    def __init__(self, atoms, calculator = []):
        # initialize log file
        logging.basicConfig(filemode='a', # append
                            level = logging.INFO,
                            datefmt='%d-%b-%y %H:%M:%S')
        self.logger = logging.getLogger('fixVasp_logger')
        formatter = logging.Formatter('%(asctime)s - [%(levelname)s] - %(message)s')
        fl = logging.FileHandler("fixvasp.log")
        fl.setFormatter(formatter)
        if not len(self.logger.handlers):
            self.logger.addHandler(fl)
        self.logger.info(' Start FixVasp application')
        # give calculator object
        self.calculator = calculator
        self.atoms = atoms
        
    
    def fix_if_needed(self):
        """ Run all steps to fix calculation """
        # first check if directory has OUTCAR file
        if self.check_new_calc_directory():
            self.logger.info(' Directory has no OUTCAR file, starting new calculation')
            return self.atoms, self.calculator
        
        self.find_errors()
        self.adjust_calculator()
        
        return self.atoms, self.calculator
        
    def fix_if_needed_NEB(self):
        """ Run all steps to fix NEB calculation """
        # first check if directory has OUTCAR file
        if self.check_new_calc_directory():
            self.logger.info(' Directory has no OUTCAR file, starting new calculation')
            return self.atoms, self.calculator

        self.find_errors_NEB()
        self.adjust_calculator()

        return self.atoms, self.calculator

    def check_new_calc_directory(self):
        """ Check if directory lacks OUTCAR file indicating a new calculation """
        # if OUTCAR does not exist, it is assumed to be a new calculation
        if Path('OUTCAR').is_file():
            return False

        # NEB calculation started if neb.traj exists
        if Path('neb.traj').is_file():
            return False
        
        return True
            
    def find_errors(self):
        """ Find common errors by reading files in folder """
        
        self.calc_dic = self.calculator.todict()
        
        # -------------------- TIMEOUT/IONIC STEP ISSUES ----------------------
        if self.max_nsw_reached():
            self.logger.info(' Maximum number of ionic steps was reached.' +
                             ' Restarting calculation from last image')
            
            self.atoms = read('OUTCAR')
            self.fix_task = {'_set': {'istart':1}}
        
        if self.calculation_timed_out():
            self.logger.info(' Error file indicates calculation timed out.' +
                             ' Restarting calculation from last image.')
            ### VASP RELAXATIONS
            self.fix_task = {'_set': {'istart':1}} # in case WAVECAR actually exists
            try:
                self.atoms = read('OUTCAR')
                # if that was succesful we copy the initial OUTCAR file to keep track
                n_restart = self.get_n_restart()
                copy2('OUTCAR',f'OUTCAR_RESTART_{n_restart}')

            except IndexError:
                self.atoms = read('POSCAR')
                n_restart = self.get_n_restart()
                copy2('POSCAR',f'POSCAR_RESTART_{n_restart}')
                self.logger.info(' Cannot read atoms object from OUTCAR, use POSCAR' +
                                 ' Calculation might timed out before first ionic step converged.')
            # keep track of indices
            if n_restart == 0:
                copy2('ase-sort.dat',f'ase-sort_{n_restart}.dat')
            # might need an additional rattling of atoms, since wavefunctions
            # will be initalized randomly if no WAVECAR exists
                
        # ---------------------- OUT OF MEMORY --------------------------------
        if self.out_of_memory():
            self.logger.info(' Out of memory. ' +
                             ' Check if memory was increased')
            if self.memory_increased():
                # One could think about initializing from WAVECAR here
                self.fix_task = {}

        # ------------------ VASP specific errors -----------------------------
        # look at vasp.out file and find errors here
        filename ='vasp.out'
        vaspout_errs = self.check_out_file(filename = filename,
                                           errors =_vasp_out_errors)
        if vaspout_errs:
            self.handle_vaspout_errs(vaspout_errs, filename)
            
        # if it has no fix_task attribute, no errors were found
        if not hasattr(self, 'fix_task'):
            self.logger.warning(' No Errors were found although OUTCAR exists.' +
                                ' I cannot fix this error yet')
            raise FixVaspError("FixVaspCalculation cannot fix/find the error that occured")
    
    def handle_vaspout_errs(self, vaspout_errs, filename):
        """ Action given the found errors in the vasp.out file """
        if len(vaspout_errs) > 1:
            handle_errors = self.can_handle_multiple_errors(vaspout_errs)
            if handle_errors:
                vaspout_errs = handle_errors
            else:
                msg = (" More than one error found in vasp.out file."
                       " I cannot handle more than one error for now")
                self.logger.warning(msg)
                msg = ("FixVasp found {} errors, can only handle one".format(
                    len(vaspout_errs))
                       )
                raise FixVaspError(msg)
        for err in vaspout_errs:
            self.fix_task = _vasp_error_adjustments[err]
            self.logger.info(f" {filename} showed error: '{err}'." +
                             f' Adjusting calculator: {_vasp_error_adjustments[err]}')
            
    def find_errors_NEB(self):
        """ Looks for specific VASP errors """
        self.calc_dic = self.calculator.todict()
        
        # -------------------- TIMEOUT ----------------------

        if self.calculation_timed_out():
            self.logger.info(' Error file indicates calculation timed out.' +
                             ' Restarting calculation from last images.')
            ### VASP RELAXATIONS
            self.fix_task = {'_set': {'istart':1}} # in case WAVECAR actually exists

            N = self.get_N_NEB()

            self.atoms = read(f'neb.traj@-{N}:')
            # if that was succesful we copy the initial OUTCAR file to keep track
            n_restart = self.get_n_restart()
            copy2('neb.traj',f'neb_RESTART_{n_restart}.traj')

        # ------------------ VASP specific errors -----------------------------
        # look at all vasp.out files and find errors here
        filename ='vasp.out'
        all_f_in_tree = self.get_f_in_tree(filename = filename)
        vaspout_errs = []
        for f in all_f_in_tree:
            vaspout_errs_temp = self.check_out_file(filename = f,
                                               errors =_vasp_out_errors)
            vaspout_errs += vaspout_errs_temp
            if vaspout_errs:
                # if a single error was found in any file, break
                break

        if vaspout_errs:
            self.handle_vaspout_errs(vaspout_errs, filename)
            
        # if it has no fix_task attribute, no errors were found
        if not hasattr(self, 'fix_task'):
            self.logger.warning(' No Errors were found although OUTCAR exists.' +
                                ' I cannot fix this error yet')
            raise FixVaspError("FixVaspCalculation cannot fix/find the error that occured")

    def can_handle_multiple_errors(self, vaspout_errs, 
                                   error_combs = _known_error_combinations):
        """ Checks if it can handle multiple errors and returns adjustment """
        errs_temp = vaspout_errs.copy()
        for key in error_combs:
            if key in errs_temp:
                errs_temp.remove(key)
                error_combs = error_combs[key]
                
                if isinstance(error_combs, str):
                    return [error_combs]
                # recursive call
                return self.can_handle_multiple_errors(errs_temp, error_combs)
                
        return False
        
    def get_f_in_tree(self,filename):
        """ returns paths to all files in tree structure """
        paths_to_f = []
        for path, dirs, files in os.walk(os.getcwd()):
            for f in files:
                if f == filename:
                    paths_to_f.append(path + '/' + f)

        return paths_to_f

    def get_N_NEB(self):
        """ get number of images in NEB path """
        return len(self.atoms)
    
    def adjust_calculator(self):
        """ Change calculator accoring to error found """

        for task in self.fix_task.keys():
            if task == '_set':
                self.calculator.set(**self.fix_task['_set'])

            if task == '_copy_file':
                file, dest_file = sorted(list(self.fix_task['_copy_file']))
                copy2(file, dest_file)

            # set atoms has to happen AFTER copy file, otherwise ase-sort.dat destroyed
            if task == '_atoms':
                self.atoms = read(list(self.fix_task['_atoms'])[0])


        return

    def max_nsw_reached(self):
        """ Check if maximum number of ionic steps reached"""
        
        if self.calculator.read_number_of_ionic_steps() == self.calc_dic['nsw']:
            return True
        return False
    
    def return_latest_file(self, endstr='.err'):
        """ returns latest file in folder 
        
        Notes
        --------
        In timeout case a new error file exists connected to the new job
        also in cases where the calculation has not been started automatically
        it should be checked first if that happened, otherwise it is trying
        to fix an error within the same job (automatic error handler)
        """
        files = [(os.path.getmtime(fs),fs) for fs in os.listdir() 
                     if fs.endswith(endstr)]
        try:
            latest_file = sorted(files)[-2][1]
        except IndexError:
            latest_file = sorted(files)[-1][1]
        return latest_file
    
    def calculation_timed_out(self):
        """ Check if calculation timed out and restart, WAVECAR does not exist! 
        
        Notes
        -----
        It looks into the latest *.err file and checks if the string 
        'DUE TO TIME LIMIT' is in there. Returns true if so, otherwise False
        
        """
        # will read the error file, have to find most recent error file first
        latest_err_file = self.return_latest_file(endstr='.err')
        with open(latest_err_file,'r') as f:
            for line in f:
                if line.rfind(ErrorStrings.time_out)> -1:
                    return True
        return False
    
    def out_of_memory(self):
        """ Checks if calculation ran out of memory """
        latest_err_file = self.return_latest_file(endstr='.err')
        with open(latest_err_file,'r') as f:
            for line in f:
                if line.rfind(ErrorStrings.out_of_memory)> -1:
                    return True
        return False
    
    def memory_increased(self):
        """ Check if memory was increased compared to last run """
        n_tasks_current = int(os.getenv('SLURM_NTASKS'))
        n_tasks_before = self.get_ntasks_outcar()
        
        if n_tasks_current <= n_tasks_before:
            msg = (f"tasks now:{n_tasks_current} lower/equal than before:{n_tasks_before}\n"
                  "Memory has probably not been increased, run on more cores\n"
                  "srun: task 0: Out Of Memory\n")
            raise FixVaspError(msg)
        return True
        
    def get_ntasks_outcar(self, path='./'):
        """ returns number of tasks from outcar file """
        with open('/'.join((path, 'OUTCAR')),'r') as f:
            for line in f:
                if line.rfind('running on')> -1:
                    task_line = line
        return int(task_line.split()[2])

    def check_out_file(self, filename = 'vasp.out', 
                       errors = _vasp_out_errors):
        """ Reads an outfile and returns errors if any match given ones
        
        Parameters
        ----------
        filename : str
        Name of error file.
        
        errors : dictionary
        Dictionary containing errors message. Example dictionary something like
        errors = {'errorAbbreviation1':'Look for this error line in filename'}.
        If 'Look for this error line in filename' matches any of the lines in 
        the file "filename, "errorAbbrevation1" will be appended to the
        all_errors_list. Idea is that error abbreviation nameing is unique.
        
        Returns
        --------
        all_errors_found : list of str
        All errors found as strings. If no errors are found an empty list is
        returned that is interpreted as False.
        """
        
        all_errors_found = list()
        
        with open(filename, 'r') as f:
            for line in f:
                # all errors in line
                errors_in_line = [item[0] for item in errors.items() if item[1] in line]
                if errors_in_line: # True if list not empty
                    # only append if errors not alread in there
                    # WARNING: if more than one error found in line, only first
                    # will be picked
                    if errors_in_line[0] not in all_errors_found:
                        all_errors_found.append(errors_in_line[0])
        
        return all_errors_found

    def get_n_restart(self):
        """ Returns for the how many'th time calculation will be restarted """
        all_restart_f = [e for e in os.listdir() if 'RESTART' in e]

        if all_restart_f:
            n_restart = sorted([n.split('_')[-1] for n in all_restart_f])[-1]
            # neb has neb_RESTART.traj formart and OUTCAR_RESTART for others
            if '.traj' in n_restart:
                n_restart = n_restart.split('.')[0]
            n_restart = int(n_restart)
            n_restart += 1
        else:
            n_restart = 0

        return n_restart

# Exceptions
class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class FixVaspError(Error):
    """Exception raised for errors in the FixVaspApplication

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message='Do not fix error twice'):
        self.message = message
        
if __name__=="__main__":
    from ase.calculators.vasp import Vasp2
    calc = Vasp2(restart = True)
    atoms = read('OUTCAR')
    fixvasp = FixVaspCalculation(atoms,calc) # initialize
    fixvasp.fix_if_needed() # run all steps
