#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Thu Mar 14 11:15:55 2019

Script for reading input structures for DFT relaxations

@author: Felix Bölle <feltbo@dtu.dk>
'''
import numpy as np

from ase.db import connect

def get_2D_structure_from_cmr2Ddb(prototype, material):
    ''' Retrieves a 2D structure from the CMR 2D-database
    
    Returns
    --------
    atoms : ASE-atoms object
    ASE-atoms object according to prototype and material input if existing
    
    Raises
    ------
    KeyError
    If no entry is found after looking at all possible magnetic states an 
    error will be raised, stating the structure does not exist in the CMR-DB
    
    Notes
    -----
    data base can be found and downloaded here:
    https://cmr.fysik.dtu.dk/c2db/c2db.html
    does return 0 magnetic moments on all atoms
    '''
    #connect to database
    db = connect('/home/energy/feltbo/databases/c2db.db')
   
    # MoSSe is MoSe or 2H-phase and BiTe I= CdI2 = 1T-phase 
    prototypes = [prototype]
    if prototype == "MoSSe":
        prototypes += ["MoS2"]
    if prototype == "BiTeI":
        prototypes += ["CdI2"]
    
    for prototype in prototypes:
        for magstate in ['NM','FM','AFM']:
            try:
                atoms = db.get_atoms(prototype = prototype, formula = material,magstate=magstate)
                atoms.set_initial_magnetic_moments(np.zeros(len(atoms)))
                return atoms
            except KeyError:
                continue
    raise KeyError('No structure in c2db for {} - {}'.format(prototype,material))
    

def create_janus_from_cmr2Ddb(el_m,el_i,el_o,prototype,atoms):
    '''Uses janus structure information to build an averaged lattice from the
    constituent 2D monolayer materials.
    
    Parameters
    ----------
    el_i : string
    Element that will be on the inside of the tube after wrapping
    
    el_o : string
    Element that will be on the outside of the tube after wrapping
    
    el_m : string
    Element that is in the middle of the janus structure
    
    prototype : string
    Prototype for the 2D sheet following CMR prototype convention
    
    atoms : ASE-atoms object
    Atoms object containing structural information about 2D sheet
    
    Returns
    -------
    atoms : ASE-atoms object
    Newly created janus sheet structure
    
    Notes
    -------
    E.g. el_m = Mo, el_i = O,el_o = S, will find the lattices a1 (MoO2) and
    a2 (MoS2) and calculates the average a_av = (a1 + a2)/2. Lattice constant
    is the metal metal bond distance. Returns scaled atoms object
    
    '''
    formula = ''.join([el_m,el_i,'2'])
    atoms_small = get_2D_structure_from_cmr2Ddb(prototype, formula)
    a_small = atoms_small.get_cell()[0][0]

    formula = ''.join([el_m,el_o,'2'])
    atoms_large = get_2D_structure_from_cmr2Ddb(prototype, formula)
    a_large = atoms_large.get_cell()[0][0]
    
    a_av = (a_small + a_large)/2

    cell = atoms.get_cell()
    atoms.set_cell(cell * (a_av/cell[0][0]),scale_atoms = True)
    return atoms

    
