#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Thu Mar 14 11:15:55 2019

Script for storing functions that manipulate 2D Sheets using ASE

@author: Felix Bölle <feltbo@dtu.dk>
'''
import numpy as np

from ase import Atoms

from nts.nanotube_tools import decomposed_material_string

def create_MXY_janus_from_MX2_sheet(atoms,S_m,S_i,S_o):
    '''Creates a MXY sheet given a 2D-sheet with formula MX2
    
    Parameters
    ------------
    atoms  : ASE-atoms object 
    prototype 2D sheet atoms object containing the same element inside and outside
    
    S_o : string
    Chemical symbol for element in outer layer in wanted Janus sheet
    
    S_m : string 
    Chemical symbol for element in middle layer in wanted Janus sheet
    
    S_i : string 
    Chemical symbol for element in inner layer  in wanted Janus sheet
    
    Returns
    -------
    atoms : ASE-atoms object
    Janus sheet structure. Only the element symbols have been changed. No
    changes to the structure itself are made!
    
    Notes
    ------
    e.g. S_o = 'Se',S_m = 'Mo',S_i = 'S' leads to a MoSSe sheet with sulphur
    occupying one side and Selenite occupying the other side of the sheet.
    It is all based on the fact that the vacuum is along the z-axis.
    
    '''
    atoms.center()
    z_cen = atoms.get_cell()[-1][-1]/2 # assumes that atoms are centered

    # get all distances sorted
    distances = np.array(sorted([[atom.z-z_cen,atom.index] for atom in atoms]))
    ind_i = distances[:int(len(atoms)/3),1]
    ind_m = distances[int(len(atoms)/3):2*int(len(atoms)/3),1]
    ind_o = distances[2*int(len(atoms)/3):,1]

    # now get symbol list with new elements
    indices = [atom.index for atom in atoms]
    new_symbols = list()
    for ind in indices:
        if ind in ind_i:
            new_symbols.append(S_i)
        elif ind in ind_m:
            new_symbols.append(S_m)
        elif ind in ind_o:
            new_symbols.append(S_o)
    atoms.set_chemical_symbols(new_symbols)
    
    return atoms

def check_correct_XY_order(atoms_2d,el_i,el_o):
    ''' Function that check if the inside element is on the bottom of a sheet
   
    Parameters
    --------
    el_i : string
    Element that will be on the inside of the tube after wrapping
    el_o : string
    Element that will be on the outside of the tube after wrapping
    atoms_2d : ASE-atoms object
    ASE atoms object containig structural information about 2D sheet
    
    Returns
    -------
    atoms_2d : ASE-atoms object
    Returns correct ordered sheet that will make sure that the tube
    will be wrapped correctly
    
    Notes
    ------
    This only works for 2D janus sheets with vacuum along the z-axis!!!


    '''
    for i,sym in enumerate(atoms_2d.get_chemical_symbols()):
        if sym == el_i:
            z_pos_i = atoms_2d.get_positions()[i][2] # z-position
            #cov_i = covalent_radii[atomic_numbers[el_i]]
        elif sym == el_o:
            z_pos_o = atoms_2d.get_positions()[i][2]
            #cov_o = covalent_radii[atomic_numbers[el_o]]
    
    # swap if outside element sits on top z position, will be inside otherwise
    if z_pos_o > z_pos_i:
        atoms_2d.rotate(180,'x',center = (0,0,0))
        atoms_2d.wrap()
        atoms_2d.center(vacuum = 8, axis = [2])
    
    return atoms_2d

def hexagonal_cell_to_orthorhombic(atoms):
    '''This carves out a rectangular cell from the repeated hexagonal unit cell'''
    #Repeat hexagonal unit cell a sufficient number of times
    atoms = atoms.repeat([6,1,1])
    atoms = atoms.repeat([1,6,1])
    
    #Get position of reference atom chosen as atom #0
    error = 3 #Round all positions to 3 decimals so they may be compared
    x0 = round(atoms[0].x,error)
    y0 = round(atoms[0].y,error)
    z0 = round(atoms[0].z,error)
    symbol = atoms[0].symbol

    x_positions = []
    y_positions = []

    for atom in atoms[1:]:

        if atom.symbol == symbol and round(atom.y,error) == y0 and round(atom.z,error) == z0: 
            x_positions.append(atom.x)
    
        if atom.symbol == symbol and round(atom.x,error) == x0 and round(atom.z,error) == z0: 
            y_positions.append(atom.y) 

    x  = min(x_positions) #Get x coordinate of first repetition of reference atom along x
    y = min(y_positions) #Get y coordinate of first repetition of reference atom along y

    #Create atoms object of atoms inside the boundaries of the orthorhombic cell
    atoms_orthorhombic = Atoms()
    for atom in atoms:
        if round(atom.x,error) >= x0 and round(atom.x,error) < round(x,error):
            if round(atom.y,error) >= y0 and round(atom.y,error) < round(y,error):
                atom.x = atom.x - x0
                atom.y = atom.y - y0
                atoms_orthorhombic.append(atom)

    cell = [[x-x0,0,0],[0,y-y0,0],atoms.cell[2]] #Define orthorhombic cell based on values x and y
    atoms_orthorhombic.set_cell(cell)

    return atoms_orthorhombic

def janus_sheet_alternate(atoms, stype, material):
    """ Create an alternating sheet for janus type 2-D materials (HfBrS prototype)
    
    Parameters
    -----------
    atoms : ASE-atoms object
    2-D (asymmetric) janus atoms object with Y-M-X structure type
    
    stype: string
    type of alternating sheet (currently 1 or 2).
    
    material : string
    material string given as formula
    
    Returns
    ----------
    atoms : ASE-atoms object
    2-D (symmetric) alternating sheet with alternating Y-X on both sides given
    the wanted alternating prototype (stype)
    """

    el_m,el_i,el_o = decomposed_material_string(material)
    atoms = atoms.repeat([2,1,1])
    indeces_X = [atom.index for atom in atoms if atom.symbol == el_i]
    indeces_Y = [atom.index for atom in atoms if atom.symbol == el_o]
        
    # exchange indeces for alternating sheet
    chem_sym = atoms.get_chemical_symbols()
    if stype == '2D_Sheet_alternating_1':
        chem_sym[indeces_X[0]] = el_o
        chem_sym[indeces_Y[0]] = el_i
    elif stype == '2D_Sheet_alternating_2':
        chem_sym[indeces_X[0]] = el_o
        chem_sym[indeces_Y[1]] = el_i
    atoms.set_chemical_symbols(chem_sym)

    return atoms
