#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Thu Mar 14 11:15:55 2019

Script for storing functions that manipulate and create nanotubes using ASE

@author: Felix Bölle <feltbo@dtu.dk>
'''

from __future__ import print_function
import math
from collections import Counter
import numpy as np
from contextlib import contextmanager
import os,sys
import re

from ase.io import read, write
from ase import Atoms
from ase.utils import convert_string_to_fd

def decomposed_material_string(material):
    ''' Decompose material string into a list of elements
    
    Parameters
    --------
    material : string
    Materials string in any chemical formula composition
    
    Returns
    -------
    element_list : list
    List containing the elements extracted from the material string
    
    Examples
    -------
    >>> material = Fe2OSe
    >>> elements = decomposed_material_string(material)
    ['Fe','O','Se']
    
    '''
    
    return re.findall('[A-Z][^A-Z]*', material)

def check_circular(atoms):
    ''' Function that checks if nanotube is circular or not
    
    Notes
    -----
    Works only for nanotubes, where an atomic species appears only once in 
    the repetitive unit i.e. MoS2 (Mo), GeS2 (Ge), MnO (Mn or O) ...
    
    ''' 
    symbol = Counter(atoms.get_chemical_symbols()).most_common()[-1][0] #Returns least common atom species in the nanotube
    x_center = atoms.get_center_of_mass()[0]
    y_center = atoms.get_center_of_mass()[1]
    
    distances = []
    angles = []
    angular_diffs = []
    for atom in atoms:
        if atom.symbol == symbol:
            distances.append(math.sqrt((atom.x-x_center)**2+(atom.y-y_center)**2))
            angles.append(math.atan2(atom.y-y_center,atom.x-x_center))
    
    angles.sort()
    for i in range(len(angles[:-1])):
        angular_diffs.append(angles[i+1]-angles[i])
    angular_diffs.append((angles[0]+math.pi)+(math.pi-angles[-1])) 
    
    # filter out 0 due to repeating cell
    angular_diffs = np.array(angular_diffs)
    angular_diffs = angular_diffs[angular_diffs > 1e-4] 
     
    stdev_distances = np.std(distances)
    stdev_angles = np.std(angular_diffs)
    if stdev_distances < 0.05 and stdev_angles < 0.05:
        return True

    # case if different angles in between atoms but still circular, hexagonal symmetry!
    if stdev_distances < 0.05 and stdev_angles > 0.05:
        ang_sort = np.sort(angular_diffs)
        stdev_1 = np.std(ang_sort[:int(len(ang_sort)/2)])
        stdev_2 = np.std(ang_sort[int(len(ang_sort)/2):])
        if stdev_1 < 0.05 and stdev_2 < 0.05:
            return True

    return False
    
def measure_diameter(atoms):
    
    distances = []
    x_center = atoms.get_center_of_mass()[0]
    y_center = atoms.get_center_of_mass()[1]
    
    for atom in atoms:
        distances.append(math.sqrt((atom.x-x_center)**2+(atom.y-y_center)**2))

    diameter = 2*np.mean(distances)
    return diameter

def wrap_2D(atoms,name,kind,n,c):
    ''' Wraps a 2D sheet into a nanotube
    
    Parameters
    --------
    atoms : ASE-atoms object   
    Atoms object has to contain the 2D sheet in an orthorombic cell shape.
    Therefore, the unit cell of the input atoms object contains twice the amount
    of atoms, compared to the hexagonal unit cell
    kind  : string
    Nanotube kind: so far only 'zigzag' (m = 0) and 'armchair' (m =  n) 
    are implemented
    n     :  int 
    number of repititions of atoms in unit cell following standard nanotube
    convention
    c     : int
    helical rise/chirality following standard nanotube convention
    
    Returns
    -------
    atoms : ASE-atoms object
    Atoms structure of the wanted nanotube
    
    '''
    a = atoms.get_cell()[0][0]
    b = atoms.get_cell()[1][1]
    n = int(n)
    c = int(c)
    
    if kind == 'zigzag':
        atoms = atoms.repeat([n,1,1])
        r = (n*a)/(2*math.pi)
    if kind == 'armchair':
        atoms = atoms.repeat([1,n,1])
        r = (n*b)/(2*math.pi)
    atoms.center(vacuum=100,axis=2)
    
    zcenter = atoms.get_center_of_mass()[2]
    for i in range(len(atoms)):
        atoms[i].z = r+(zcenter-atoms[i].z)
    
    for i in range(len(atoms)):
        if kind == 'zigzag':
            l = atoms[i].x
            rnew = atoms[i].z
            atoms[i].x = rnew*math.sin(l/r)
            atoms[i].z = atoms[i].y+c*b*l/r/(2*math.pi)
            atoms[i].y = rnew*math.cos(l/r)
        elif kind ==  'armchair':
            l = atoms[i].y
            rnew = atoms[i].z
            atoms[i].y = rnew*math.sin(l/r)
            atoms[i].z = atoms[i].x+c*a*l/r/(2*math.pi)
            atoms[i].x = rnew*math.cos(l/r)
        else:
            print('Not implemented nanotube type')
            sys.exit()
    
    cell = atoms.get_cell()
    if kind == 'zigzag':
        atoms.set_cell([cell[0][0],cell[2][2],cell[1][1]])
        name += '-z'
    if kind ==  'armchair':
        atoms.set_cell([cell[2][2],cell[1][1],cell[0][0]])
        name += '-a'
    
    atoms.center(vacuum=8,axis=[0,1])
    if kind == 'zigzag':
        atoms.set_cell(atoms.get_cell()*[1.1,1.1,1],scale_atoms=True)
    
    return atoms


def create_janus_MXY_tube_from_MX2_tube(atoms,S_m,S_i,S_o):
    '''Creates a MXY janus nanotube given a tube with formula MX2
    
    Parameters
    ------------
    atoms  : ASE-atoms object 
    prototype nanotube atoms object containing the same element inside and outside
    
    S_o : string
    Chemical symbol for element in outer layer in wanted Janus nanotube
    
    S_m : string 
    Chemical symbol for element in middle layer in wanted Janus nanotube
    
    S_i : string 
    Chemical symbol for element in inner layer  in wanted Janus nanotube
    
    Returns
    -------
    atoms : ASE-atoms object
    Janus nanotube structure. Only the element symbols have been changed. No
    changes to the structure itself are made!
    
    Notes
    ------
    e.g. S_o = 'Se',S_m = 'Mo',S_i = 'S' leads to a MoSSe nanotube with sulphur
    occupying the innermost layer and Selenite sitting outside of the tube
    
    '''
    x_cen,y_cen,_ = atoms.get_center_of_mass()
    # get all distances sorted
    distances = np.array(sorted([[np.sqrt((atom.x-x_cen)**2+(atom.y-y_cen)**2),atom.index] for atom in atoms]))
    ind_i = distances[:int(len(atoms)/3),1]
    ind_m = distances[int(len(atoms)/3):2*int(len(atoms)/3),1]
    ind_o = distances[2*int(len(atoms)/3):,1]
    # now get symbol list with new elements
    indices = [atom.index for atom in atoms]
    new_symbols = list()
    for ind in indices:
        if ind in ind_i:
            new_symbols.append(S_i)
        elif ind in ind_m:
            new_symbols.append(S_m)
        elif ind in ind_o:
            new_symbols.append(S_o)
    atoms.set_chemical_symbols(new_symbols)
    return atoms

    
def get_2d_row_from_db(prototype,formula,db):
    for magstate in ['NM','FM','AFM']:
        try:
            row = db.get(prototype = prototype, formula = formula,magstate=magstate)
            return row
        except KeyError:
            continue
    raise KeyError('No structure in c2db for {} - {}'.format(prototype,formula))

def get_stable_prototype_dic():
    ''' Returns stable nanotube prototype dictionary for Janus structures '''

    ### STABLE PROTOTYPE DATA
    dic_stable_prototype = {'Ti':{'S':{'Se':'CdI2','Te':'CdI2'},
                                  'Se':{'Te':'BiTeI'},
                                  'O':{'Se':'CdI2','Te':'CdI2','S':'CdI2'},
                                  'Cl':{'Br':'MoSSe','I':'MoSSe'},
                                  'Br':{'I':'MoSSe'}},
                            'Ge':{'S':{'Se':'CdI2','Te':'CdI2'},
                                 'Se':{'Te':'BiTeI'},
                                 'O':{'Se':'CdI2','Te':'CdI2','S':'CdI2'},
                                 'Cl':{'Br':'BiTeI','I':'BiTeI'},
                                 'Br':{'I':'BiTeI'}},
                            'V':{'S':{'Se':'CdI2','Te':'CdI2'},
                                 'Se':{'Te':'MoSSe'},
                                 'O':{'Se':'CdI2','Te':'CdI2','S':'CdI2'},
                                 'Cl':{'Br':'BiTeI','I':'BiTeI'},
                                 'Br':{'I':'BiTeI'}},
                            'Fe':{'S':{'Se':'CdI2','Te':'CdI2'},
                                  'Se':{'Te':'MoSSe'},
                                  'O':{'Se':'CdI2','Te':'CdI2','S':'CdI2'}},
                            'Hf':{'S':{'Se':'CdI2','Te':'CdI2'},
                                  'Se':{'Te':'BiTeI'},
                                  'O':{'Se':'CdI2','Te':'CdI2','S':'CdI2'},
                                  'Cl':{'Br':'MoSSe','I':'MoSSe'},
                                  'Br':{'I':'MoSSe'}},
                            'Sn':{'S':{'Se':'CdI2','Te':'CdI2'},
                                  'Se':{'Te':'BiTeI'},
                                  'O':{'Se':'CdI2','Te':'CdI2','S':'CdI2'}},
                            'Mo':{'S':{'Se':'MoS2','Te':'MoS2','O':'MoS2'},
                                  'Se':{'Te':'MoSSe','O':'MoS2'},
                                  'Te':{'O':'MoSSe'},
                                  'O':{'Se':'MoS2','Te':'MoS2','S':'MoS2'},
                                  'Cl':{'Br':'BiTeI','I':'BiTeI'},
                                  'Br':{'I':'BiTeI'}},
                            'Zr':{'S':{'Se':'CdI2','Te':'CdI2'},
                                  'Se':{'Te':'BiTeI'},
                                  'O':{'Se':'CdI2','Te':'CdI2','S':'CdI2'},
                                  'Cl':{'Br':'MoSSe','I':'MoSSe'},
                                  'Br':{'I':'MoSSe'}},
                            'W':{'S':{'Se':'MoS2','Te':'MoS2'},
                                 'Se':{'Te':'MoSSe'},
                                 'O':{'Se':'MoS2','Te':'MoS2','S':'MoS2'},
                                 'Cl':{'Br':'BiTeI','I':'BiTeI'},
                                 'Br':{'I':'BiTeI'}},
                            'Ta':{'S':{'Se':'MoS2','Te':'MoS2'},
                                  'Se':{'Te':'MoSSe'},
                                  'O':{'Se':'MoS2','Te':'MoS2','S':'MoS2'},
                                  'Cl':{'Br':'BiTeI','I':'BiTeI'},
                                  'Br':{'I':'BiTeI'}},
                            'Nb':{'S':{'Se':'MoS2','Te':'MoS2'},
                                  'Se':{'Te':'MoSSe'},
                                  'O':{'Se':'MoS2','Te':'MoS2','S':'MoS2'},
                                  'Cl':{'Br':'BiTeI','I':'BiTeI'},
                                  'Br':{'I':'BiTeI'}},
                            'Cr':{'S':{'Se':'MoSSe','Te':'MoSSe'},
                                  'Se':{'Te':'MoSSe'},
                                  'O':{'Se':'MoSSe','Te':'MoSSe','S':'MoSSe'},
                                  'Cl':{'Br':'BiTeI','I':'BiTeI'},
                                  'Br':{'I':'BiTeI'}},
                            'Bi':{'S':{'Cl':'BiTeI','Br':'BiTeI','I':'BiTeI'},
                                  'Se':{'Cl':'BiTeI','Br':'BiTeI','I':'BiTeI'},
                                  'Te':{'Cl':'BiTeI','Br':'BiTeI','I':'BiTeI'}},
                            'Sb':{'S':{'Cl':'BiTeI','Br':'BiTeI','I':'BiTeI'},
                                  'Se':{'Cl':'BiTeI','Br':'BiTeI','I':'BiTeI'},
                                  'Te':{'Cl':'BiTeI','Br':'BiTeI','I':'BiTeI'}},
                            'As':{'S':{'Cl':'BiTeI','Br':'BiTeI','I':'BiTeI'},
                                  'Se':{'Cl':'BiTeI','Br':'BiTeI','I':'BiTeI'},
                                  'Te':{'Cl':'BiTeI','Br':'BiTeI','I':'BiTeI'}}}
    return dic_stable_prototype




