#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Thu Mar 14 14:05:55 2019

Script for doing fine k-mesh calculations for 2D sheets.

@author: August Edwards <aegm@dtu.dk>
'''


from ase.io import read,write

from nts.vasp_relaxation_setups import get_custom_kpts, get_vasp_calc_initial_relaxation_sheet, setup_calc_dipole_sheet
from nts.vasp_helper import get_number_of_electrons
from pathlib import Path


def fine_k_mesh_2D():

    #--------------------- STEP 1: retrieve Structure -------------------------
    atoms_2d = read('../vasp_rx/OUTCAR')

    #-------------------------- STEP 2: SETUP CALCULATION ----------------------
    calc = get_vasp_calc_initial_relaxation_sheet(atoms_2d)
    calc = setup_calc_dipole_sheet(atoms_2d, calc)
    n_electrons = get_number_of_electrons(path = '../vasp_rx') #deduce number of electron from previous relaxation
    nbands = n_electrons//2 + 100 #100 empty bands
    kpts = get_custom_kpts(atoms_2d.get_cell(), density_criterion = 100) 
    kpts = [kpts[0], kpts[1], 1] # vacuum along z-axis
    calc.set(ibrion=-1)
    calc.set(nsw=0)
    calc.set(nedos = 1000)
    calc.set(nbands = nbands)
    calc.set(lwave = False)
    calc.set(kpts = kpts)
    calc.set(ismear = -5)
    calc.set(lvhar = True)
    calc.set(lorbit = 12)

    #-------------------------- STEP 3: DO CALCULATION ----------------------
    atoms_2d.set_calculator(calc)
    atoms_2d.get_potential_energy()
    


if __name__=='__main__':
    state = fine_k_mesh_2D()
