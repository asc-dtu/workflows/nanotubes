#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Thu Mar 14 14:05:55 2019

Script for relaxing 2D Sheets using VASP and ASE

@author: Felix Bölle <feltbo@dtu.dk>
'''
import os

from nts.nanotube_tools import decomposed_material_string
from nts.structure_input import (get_2D_structure_from_cmr2Ddb,
                                 create_janus_from_cmr2Ddb)
from nts.sheet_tools import (create_MXY_janus_from_MX2_sheet,
                             janus_sheet_alternate)
from nts.vasp_relaxation_setups import (get_vasp_calc_initial_relaxation_sheet,
                                        initial_relaxation_routine_vasp_sheet)


def relax_2d():
    #--------------------- STEP 1: retrieve Structure -------------------------
    # get information from folder name
    cwd = os.getcwd()
    properties = cwd.split('/')
    prototype = properties[-4]
    material = properties[-3]
    sheettype = properties[-2]

    # connect to database
    element_list = decomposed_material_string(material)
    dipole_correction = False  # switched of by default
    if len(element_list) > 2:  # assumes it is janus structure for now
        dipole_correction = True  # assumes asymmetric sheet
        el_m, el_i, el_o = element_list
        try:  # it may exist in the 2D database
            atoms_2d = get_2D_structure_from_cmr2Ddb(
                prototype,material)
        except:
            formula = ''.join([el_m, el_i, '2'])  # no guarantee that this material exists in the 2D database !!!
            atoms_2d = get_2D_structure_from_cmr2Ddb(
                prototype, formula)
            atoms_2d = create_janus_from_cmr2Ddb(
                el_m, el_i, el_o, prototype, atoms_2d)
            atoms_2d = create_MXY_janus_from_MX2_sheet(
                atoms_2d, el_m, el_i, el_o)
    else:
        try:
            atoms_2d = get_2D_structure_from_cmr2Ddb(prototype, material)
        except KeyError:
            # start from which material?
            material_match = {'GeI2': 'GeBr2'}
            atoms_2d = get_2D_structure_from_cmr2Ddb(
                prototype, material_match[material])
            el_string = decomposed_material_string(material)[-1]
            el_new = ''.join([s for s in el_string if s.isalpha()])
            el_string = decomposed_material_string(material_match[material])[-1]
            el_old = ''.join([s for s in el_string if s.isalpha()])
            chem_syms = atoms_2d.get_chemical_symbols()
            chem_syms = [el_new if el_old == el else el for el in chem_syms]
            atoms_2d.set_chemical_symbols(chem_syms)

    # check for alternating prototype
    if sheettype == '2D_Sheet_alternating_1' or sheettype == '2D_Sheet_alternating_2':
        atoms_2d = janus_sheet_alternate(
            atoms_2d, sheettype, material)  # in the making

    # insert vacuum and center atoms
    atoms_2d.center(vacuum=8, axis=2)
    atoms_2d.set_pbc = ([True, True, True])

    # -------------------------- STEP 2: SETUP RELAXATION ----------

    calc_vasp = get_vasp_calc_initial_relaxation_sheet(atoms_2d)

    # ------------------------- STEP 3: RELAX STRUCTURE ------------
    status = initial_relaxation_routine_vasp_sheet(
        atoms_2d, calc_vasp, dipole_correction=dipole_correction)
 
    return status

if __name__ == '__main__':
    state = relax_2d()
    if state == 'nsteps':
        raise ValueError('Maximum number of ionic steps reached.')
    elif state == 'forces':
        raise ValueError('Forces not converged.')
    elif state == 'counter':
        raise ValueError('Maximum number of restarts (10) reached.')
