# evaluate stability, heat of formation AND convex hull

'''It uses the energies from the OQMD directly, but relaxation scheme probably not axactly the same
OQMD uses (I think): xc = 'PBE', Ecut = 520 eV (choosing 'static' as calculation type)
isif = 3 and then single isif=2, ismear = -5, sigma = .2, ediff = 0.0001, kpoints = enough I hope
They give the INCAR file for each relaxation, e.g.
http://oqmd.org/analysis/calculation/1335847

For now I use those energies, otherwise use our relaxation scheme using (but first adjusting)

relax_bulk_vasp.py takes care of it - reads in all materials
'''

import matplotlib.pyplot as plt
import pickle
import os
import re
from collections import Counter

from ase.db import connect
from ase.io import read
from ase.phasediagram import PhaseDiagram


# dictionaries for constant values
# ZPE values were taken from Rossmeisel et al. (2005)
#http://dcwww.camd.dtu.dk/~jross/Welcome_files/Rossmeisl_CP_2005.pdf
ZPE = {'H2':0.27,'H2O':0.56,'0.5O2':0.05}
# energies calculated using molecules_vasp.py script
E = {'H2':-6.772,'H2O':-14.224}
# formation energies at 0K
# https://atct.anl.gov/Thermochemical%20Data/version%201.122/species/?species_number=968
H_0_0K = {'H2O':-2.476}
# get oxygen with water reference, E['0.5O2'] = -4.736 compared to E_DFT['0.5O2'] = -4.931
halfE_O2 = E['H2O'] + ZPE['H2O'] - (E['H2'] + ZPE['H2'] + H_0_0K['H2O']) - ZPE['0.5O2']

# refrence databases
#db = connect('/home/energy/ivca/Nanotubes_project/bulk_reference_states.db')
#db = connect('/home/energy/ivca/Nanotubes_project/oqmd12.db')
db = connect('/home/felix/databases/oqmd12.db')

def material_decompose(material):
    '''returns elements from material string
    just like set(atoms.get_chemical_symbols())'''
    mat_el = re.findall('[A-Z][^A-Z]*', material)
    mat_sym = []
    for el in mat_el:
        mat_sym.append(''.join([i for i in el if not i.isdigit()]))
        
    return mat_sym

def n_unit_material(material):
    '''returns number of elements one unit is made of from formula'''
    mat_el = re.findall('[A-Z][^A-Z]*', material)
    n_at_unit = 0
    for el in mat_el:
        if any([i for i in el if i.isdigit()]):
            n_at_unit += int(''.join([i for i in el if i.isdigit()]))
        else:
            n_at_unit += 1
            
    return n_at_unit
                
def formation_energy(atoms,dbref=False):
    energy = atoms.get_potential_energy()
    count = Counter(atoms.get_chemical_symbols())
    e0 = 0
    
    for symbol,n in count.items():
        if symbol == 'O': # calculated with water reference, calculation at top of script
            e0 += n*halfE_O2
        else:
            if dbref:
                e0 += n*db.get(elements=symbol,u=False).oqmd_energy_pa
            else:
                e0 += n*db.get(elements=symbol).energy_per_atom
            
    return energy - e0

def formation_energy_ref(row):
    energy = row.oqmd_energy_pa * len(row.numbers)
    count = Counter(row.toatoms().get_chemical_symbols())
    e0 = 0
    
    for symbol,n in count.items():
        if symbol == 'O': # calculated with water reference, calculation at top of script
            e0 += n*halfE_O2
        else:
            e0 += n*db.get(elements=symbol,u=False).oqmd_energy_pa

    return energy - e0

def convex_hull(material,f_energy,verbose=False,plot=False,ThreeD=False,dbref=True):
    '''checks how the tube performs on the convex hull
    right now only elemental and binary compositions considered
    f_energy = formation energy of tube per unit !!! (not atom, ASE is doing that)'''
    # similar to https://gitlab.com/camd/cmr/blob/master/c2db/convex_hull.py
    refs = []
    mat_dec = material_decompose(material)

    for row in db.select(u=False):
        mat_db = material_decompose(row.formula)
        if all([el in mat_dec for el in mat_db]):
            if len(re.findall('[A-Z][^A-Z]*', row.formula)) == 1: #elemental
                refs.append((''.join([i for i in row.formula if not i.isdigit()]),0))
            else: #else it is binary/ternary etc.
                if dbref:
                    refs.append((row.formula,formation_energy_ref(row)))
                else:
                    refs.append((row.formula,formation_energy(db.get_atoms(id=row.id))))
    if 'O' in material_decompose(material): # again separately since water reference is used
        refs.append(('O',0))
        
    pd = PhaseDiagram(refs,verbose=verbose) # verbose False suppresses text output
    e_form0,_,_ = pd.decompose(material) # per unit needed
    e_form0_peratom = e_form0/n_unit_material(material) # per atom needed
    e_ch = (f_energy - e_form0)/n_unit_material(material)
    
    # now include the tube
    if plot:
        refs.append((material,f_energy))
        pd = PhaseDiagram(refs,verbose=verbose)
        if ThreeD:
            pd.plot(dims=3,show=True)
            plt.draw() # not plt.show() for ubuntu 16.04 for interactive plots apparently
        else:
            pd.plot()
            plt.draw()
            
    return e_form0,e_form0_peratom,e_ch

def get_stabilities_oqmd(atoms,material,dbref=True):
    # prior to running the script, bulk energies should have been relaxed with
    # relax_bulk_vasp.py script
    dbref = True
    if dbref:
        #db = connect('/home/energy/ivca/Nanotubes_project/oqmd12.db')
        connect('/home/energy/ivca/Nanotubes_project/oqmd12.db')
    f_energy = formation_energy(atoms,dbref=dbref)
    n_units = n_unit_material(material)
    f_energy = f_energy/(len(atoms)/n_units)
    e_form0,e_form0_peratom,e_ch = convex_hull(material,f_energy)
    Hf = f_energy/n_units
    stability = {'HoF':Hf,'CH_stab':e_ch}
    if 0: # if needed store it
        with open('stability.pickle','wb') as f:
            pickle.dump(stability,f)

    return Hf, e_ch

if __name__ == '__main__':
    # test a specific structure by changing to wanted material and Nanotube OUTCAR file
    
    atoms = read('OUTCAR')
    material = os.getcwd().split('/')[-5]
    Hf, e_ch = get_stabilities_oqmd(atoms,material)

    print('HoF: ', Hf, 'e_ch: ',e_ch)

