import subprocess
import pickle
import itertools

from nts.nanotube_tools import (get_stable_prototype_dic,
                            decomposed_material_string)

def sync_done_files():
    '''Returns shell command to sync only .done files from parent directory'''
    cmd = 'rsync -a -u' # -a is archive (keep permissions, time stamps etc.), -u is update (only sync in your folder if file from parent is more recent)
    cmd += ' --include="*/"' # include folders
    cmd += ' --include="*.done"' # include only .done files
    cmd += ' --exclude="*"' # skip everything else
    cmd += ' /home/energy/ivca/Nanotubes_project/Nanotubes_data/' # sync .done files from here ...
    cmd += ' .' # ... to here
    
    return cmd

def comb_element_lists(el_lists):
    """ Combines lists of elements to give back all possible combinations 
    
    Parameters
    -------------
    el_lists : list of lists
    e.g. el_list = [['Ag','Au'],
                    ['Sn','Zn']]
    """
    return list(itertools.product(*el_lists))

#define input for the nanotubes, set multiple = True in workflow_nanotubes!
prototypes = ['from_stable_dictionary']
dic_stable_prototype = get_stable_prototype_dic()
metals = ['Ti','Ge','Mo','Zr','W','Ta','Nb','Sn','V','Fe','Hf']
groups = ['OS','OSe','OTe','STe','SSe']
kinds = ['armchair']
n_cs_arm = ['6_0','8_0','10_0','12_0','14_0'] # in that case it runs all those sizes
n_cs_zig = []
magstates = ['nm']

#material combinations to run
materials = [metal + group for metal in metals for group in groups]
metals_augfel = ['Bi','Sb','As']
groups_felix = ['SCl','SBr','SI','SeCl','SeBr','SeI','TeCl','TeBr','TeI']
#groups_august = ['ClS','ClSe','ClTe','BrS','BrSe','BrTe','IS','ISe','ITe']
metals_ivano = ['Zr','Hf','Mo','Nb','Ta','Ti','V','W']
groups_ivano = ['ClBr','ClI','BrI']
materials = materials + \
            [metal + group for metal in metals_augfel for group in groups_felix] + \
            [metal + group for metal in metals_ivano for group in groups_ivano]

# 2D parent materials screening
metals = ['Ti','Ge','Mo','Zr','W','Ta','Nb','Sn','V','Fe','Hf','Cr']
elements = ['O','S','Se','Te','Cl','Br','I']
materials1 = comb_element_lists([metals, elements])
metals = ['Bi','Sb','As']
elements = ['S','Se','Te','Cl','Br','I']
materials2 = comb_element_lists([metals, elements])
all_materials = [''.join(comb + tuple('2')) for comb in materials1 + materials2]
print(all_materials)
print(len(all_materials))
import sys;sys.exit()

# first sync folders with parent folder to avoid doing the same calculations
#sync_cmd = sync_done_files()
#subprocess.run(sync_cmd,shell=True)

for prototype in prototypes:
    for material in materials:
        if prototype == 'from_stable_dictionary':
            el_metal, el_inside, el_outside = decomposed_material_string(material)
            prototype_material = dic_stable_prototype[el_metal][el_inside][el_outside]
        twodsheet = [prototype_material,material]
        with open('current_2d','wb') as f1:
            pickle.dump(twodsheet,f1)
        subprocess.run('mq workflow workflow_2d.py',shell=True)
        
        for kind in kinds:
            if kind == 'armchair':
                n_cs = n_cs_arm
            elif kind == 'zigzag':
                n_cs = n_cs_zig
            for n_c in n_cs:
                for magstate in magstates:
                    tube = [prototype_material,material,kind,n_c,magstate]

                    with open('current_tube','wb') as f2:
                        pickle.dump(tube,f2)
                    
                    subprocess.run('mq workflow workflow_nanotubes.py',shell=True)
