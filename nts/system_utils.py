#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Thu Mar 14 11:15:55 2019

Script for storing functions that work on the operating system level

@author: Felix Bölle <feltbo@dtu.dk>
'''

from contextlib import contextmanager
import os

@contextmanager
def cd(newdir):
    ''' Change/Create working directory and catch errors '''
    prevdir = os.getcwd()
    try:
        os.mkdir(newdir)
    except OSError:
        pass
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

def get_wd(wanted_folders):
    """ Function creating folder structures given a list of strings
    
    Parameters
    ---------
    wanted_folders : list of strings
        List with strings defining the wanted folders. It will create all
        folders given the order of the list, even if they do not extist.
        The folders will be created in the current working directory.

    Returns
    ---------
    path_to_last_folder : string
        Returns absolute path to last folder in the folder structure. That
        means a path to the last entry in the wanted_folders list.
    
    See Also
    ---------
    cd : function from nanotube_tools, creates folders and catches any
         errors or exceptions

    Examples
    ---------
    >>> folders = ['1','2','3']
    >>> path_to_folder = get_wd(wanted_folders)
    >>> print(path_to_folder)
    'home/1/2/3'

    """
    cwd = os.getcwd()

    for folder in wanted_folders:
        with cd(os.path.join(cwd,folder)):
            cwd = os.getcwd()
            continue

    return cwd


