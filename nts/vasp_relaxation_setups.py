#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Thu Mar 14 13:32:12 2019

Script for storing all VASP relaxation inputs and relaxation schemes

@author: Felix Bölle <feltbo@dtu.dk>
'''
import os
import shutil
import numpy as np
from pathlib import Path


from ase.calculators.vasp import Vasp2
from ase.io import read

from nts.vasp_helper import get_number_ionic_steps_vasp
from nts.nanotube_tools import check_circular
from nts.fix_dft_calculation import FixVaspCalculation, FixVaspError

def get_custom_kpts(cell, density_criterion = 30):
    """ Creates kpoint grids with user specified density
    
    Parameters
    ----------
    cell : ndarray
        Cell dimensions of the input structure. In ASE this would equal the 
        function call cell = ase_atoms.get_cell()
    
    density_criterion : int
        The density criterion defines how many kpoints will be chosen along
        the axis. The amount of kpoints is calculated as 
        n_kpoints = int(density_criterion/a) + 1 , where a is the lenght of the 
        lattice vector in either of one of the cell directions. The + 1 ensures 
        for large cells, that there is at least 1 kpoint in each direction
        
    Returns
    -------
    kpts : narray
        Numpy array with number of kpoints in x,y and z direction
        
    Examples
    ---------
    >>> cell = np.array([[5,0,0],[0,5,0],[0,0,5]])
    >>> kpts = get_custom_kpts(cell, density_criterion = 30)
    >>> print(kpts)
    [7,7,7]
        
    """
    
    t1,t2,t3 = cell[0,:],cell[1,:],cell[2,:]

    kpts = [int(density_criterion/np.linalg.norm(t1)) + 1,
            int(density_criterion/np.linalg.norm(t2)) + 1,
            int(density_criterion/np.linalg.norm(t3)) + 1]

    return np.array(kpts)

def get_vasp_calc_initial_relaxation_tube(atoms):
    ''' VASP Calculator for initial nanotube structure relaxations '''
    
    kpts = get_custom_kpts(atoms.get_cell())
    kpts = [1, 1, kpts[2]] # vacuum in x and y direction
    
    calc_vasp = Vasp2(xc='pbe',
                     encut=550,
                     ismear=0,
                     sigma=0.2,
                     isif = 4,
                     ncore = 40,
                     algo = 'Fast',
                     kpts=kpts,
                     nsw = 250,
                     nelmin = 8,
                     ediff = 0.000005,
                     ediffg = -0.02,
                     symprec = 1e-8,
                     ibrion = 2,
                     gamma = True,
                     lwave = True, 
                     lcharg = False,
                     setups='recommended')
    
    return calc_vasp

def get_vasp_calc_initial_relaxation_sheet(atoms, dipole_z=True):
    ''' VASP Calculator for initial sheet structure relaxations '''
    
    kpts = get_custom_kpts(atoms.get_cell())
    kpts = [kpts[0], kpts[1], 1] # vacuum along z-axis
 
    calc_vasp = Vasp2(xc='pbe',
                     encut=550,
                     ismear=0,
                     sigma=0.2,
                     isif=4,
                     ncore=40,
                     algo='normal',
                     prec='accurate',
                     kpts=kpts,
                     nsw=200,
                     nelmin=8,
                     ediff=0.00001,
                     ediffg=-0.01,
                     symprec=1e-8,
                     ibrion=2,
                     gamma=True,
                     lwave=True, 
                     lcharg=False,
                     setups='recommended')
        
    return calc_vasp

def setup_calc_dipole_sheet(atoms, calc):
    """ setup the dipole correction for the sheets """
    com = atoms.get_center_of_mass(scaled=True)  # pass direct lattice coordinates to VASP!
    calc.set(ldipol=True)
    calc.set(idipol=3)  # z-direction
    calc.set(dipol=com)
    return calc

def initial_relaxation_routine_vasp_tube(atoms,calc, max_restart=3):
    """ automatically restart a calculation  if any error found
    
    Parameters
    ------------
    max_restart : int
        Number of times the FixVaspApplication should try to fix the error
    """
    n_restarts = 0
    for n_restarts in range(max_restart):
        try:
            state = _initial_relaxation_routine_vasp_tube(atoms,calc)
            return state
        except FixVaspError as e:
            raise FixVaspError(e.message)
        except MyVaspError as e:
            raise MyVaspError(e.message)
        except BaseException as e:
            print("Something went wrong with msg: " + str(e))
            n_restarts += 1
    
    msg = f"Maximum number of routine restarts reached {n_restarts}/{max_restart}"
    raise ValueError(msg)
    
def _initial_relaxation_routine_vasp_tube(atoms,calc):
    """ Relax all vasp relaxations using isif 3 in a consistent way
    
    Notes
    ------
    This routine relaxes a structure until it converges in a single point
    calculation. For convergence it is important to save the WAVECAR. Otherwise
    local structural minima due to random wavefunction initialization in VASP
    can occur (that comes from experience when relaxing Nanotubes and 2D sheets
    containing vacuum).
    More info: https://www.vasp.at/vasp-workshop/slides/accuracy.pdf
    
    """
    # check for errors to adjust calculator/atoms object
    fixvasp = FixVaspCalculation(atoms=atoms,
                                 calculator=calc)
    atoms, calc = fixvasp.fix_if_needed()
    
    atoms.set_calculator(calc)       
    atoms.get_potential_energy()
    shutil.copy('OUTCAR','OUTCAR_0')
    nsteps = get_number_ionic_steps_vasp('OUTCAR')
    
    #---------------------- ERROR HANDLING ------------------------------------
    errors_to_check = ['circular','nsteps']
    status = check_relaxation_errors(atoms, calc, errors_to_check, nsteps)
    #First, check that configuration is a tube. If not, crash the calculation
    if status in errors_to_check:
        return status
    
    #--------------------------------------------------------------------------
    
    # now rerelax the structure due to change of cell shape until it converges
    # within a single relaxation step
    counter = 1
    calc.set(istart=1) # set explicitly, but will also be set automatically since WAVECAR exists
    while nsteps > 1:
        atoms = read('OUTCAR') # read in the new cell structure and atom position before a new relaxation
        atoms.set_calculator(calc) # new plane waves adjusted to the new cell shape and based on existing WAVECAR
        atoms.get_potential_energy()
        shutil.copy('OUTCAR','OUTCAR_{}'.format(counter))
        nsteps = get_number_ionic_steps_vasp('OUTCAR')
        counter += 1

        #-------------------ERROR HANDLING ------------------------------------
        errors_to_check = ['circular','nsteps','forces','counter']
        status = check_relaxation_errors(atoms, calc, errors_to_check, 
                                         nsteps, counter = counter)
        #First, check that configuration is a tube. If not, crash the calculation
        if status in errors_to_check:
            return status

        #----------------------------------------------------------------------
        
    # remove WAVECAR file after finishing to save disk space
    os.remove('WAVECAR')
    
    return 'finished'

def initial_relaxation_routine_vasp_sheet(atoms,calc,
                                          dipole_correction=False,
                                          max_restart=3):
    """ automatically restart a calculation  if any error found
    
    Parameters
    ------------
    max_restart : int
        Number of times the FixVaspApplication should try to fix the error
    """
    n_restarts = 0
    for n_restarts in range(max_restart):
        try:
            state = _initial_relaxation_routine_vasp_sheet(
                atoms,calc,dipole_correction=dipole_correction)
            return state
        except FixVaspError as e:
            raise FixVaspError(e.message)
        except MyVaspError as e:
            raise MyVaspError(e.message)
        except BaseException as e:
            print("Something went wrong with msg: " + str(e))
            n_restarts += 1
    
    msg = f"Maximum number of routine restarts reached {n_restarts}/{max_restart}"
    raise ValueError(msg)
    
def _initial_relaxation_routine_vasp_sheet(atoms, calc,
                                           dipole_correction=False):
    """ Relax all vasp relaxations using isif 3 in a consistent way
    
    Notes
    ------
    This routine relaxes a structure until it converges in a single point
    calculation. For convergence it is important to save the WAVECAR. Otherwise
    local structural minima due to random wavefunction initialization in VASP
    can occur (that comes from experience when relaxing Nanotubes and 2D sheets
    containing vacuum).
    More info: https://www.vasp.at/vasp-workshop/slides/accuracy.pdf
    
    """
    fixvasp = FixVaspCalculation(atoms=atoms, calculator=calc)
    atoms, calc = fixvasp.fix_if_needed()
    
    atoms.set_calculator(calc)

    atoms.get_potential_energy()
    shutil.copy('OUTCAR','OUTCAR_0')
    nsteps = get_number_ionic_steps_vasp('OUTCAR')
    
    #---------------------- ERROR HANDLING ------------------------------------
    errors_to_check = ['nsteps']
    status = check_relaxation_errors(atoms, calc, errors_to_check, nsteps)
    #First, check that configuration is a tube. If not, crash the calculation
    if status in errors_to_check:
        return status
    
    #--------------------------------------------------------------------------
    
    # now rerelax the structure due to change of cell shape until it converges
    # within a single relaxation step
    counter = 1
    calc.set(istart=1) # set explicitly, but will also be set automatically since WAVECAR exists
    
    first_loop = False
    if dipole_correction:
        first_loop = True  # in case it converges already after one step, still need dipole correction

    while nsteps > 1 or first_loop:
        atoms = read('OUTCAR') # read in the new cell structure and atom position before a new relaxation
        
        if dipole_correction:
            # also turn dipole correction after pre relaxation on here, get new COM every time
            calc = setup_calc_dipole_sheet(atoms, calc)
        
        atoms.set_calculator(calc) # new plane waves adjusted to the new cell shape and based on existing WAVECAR
        atoms.get_potential_energy()
        shutil.copy('OUTCAR','OUTCAR_{}'.format(counter))
        nsteps = get_number_ionic_steps_vasp('OUTCAR')
        counter += 1

        #-------------------ERROR HANDLING ------------------------------------
        errors_to_check = ['nsteps','forces','counter']
        status = check_relaxation_errors(atoms, calc, errors_to_check, 
                                         nsteps, counter = counter)
        #First, check that configuration is a tube. If not, crash the calculation
        if status in errors_to_check:
            return status

        #----------------------------------------------------------------------
        first_loop = False
        
    # remove WAVECAR file after finishing to save disk space
    os.remove('WAVECAR')
    
    return 'finished'

def check_relaxation_errors(atoms, calc, errors, nsteps, counter = 0):
    ''' Checks for relaxation errors and returns error message if any '''
    for error in errors:
        if error == 'circular':
            if check_circular(atoms) == False:
                # remove WAVECAR, no restart necessary
                if os.path.isfile('WAVECAR'):
                    os.remove('WAVECAR')
                raise MyVaspError('Relaxed tube not circular.')
        if error == 'nsteps':
            if nsteps == calc.todict()['nsw']:
                raise MyVaspError('Maximum number of ionic steps reached.')
        if error == 'forces':
            fmax = atoms.get_forces().max()
            if fmax > abs(calc.todict()['ediffg']):
                raise ValueError('Forces not converged.')
        if error == 'counter':
            if counter == 10:
                raise MyVaspError('Maximum number of restarts (10) reached.')
            
    return 'no error'
            
# Exceptions
class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class MyVaspError(Error):
    """Exception raised for errors user defined Vasp errors

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message='MyVasp Error was raised'):
        self.message = message
