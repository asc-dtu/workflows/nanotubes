from myqueue.task import task
from pathlib import Path
import os,sys
import pickle
from nts.system_utils import cd

def get_wd(folders):
    ''' Function creates the folder structure for the calculation
    according to cwd + folders
    folders as list: e.g. folders = ['1','2','3'] creates cwd/1/2/3'''
    cwd = os.getcwd()
    for folder in folders:
        with cd(os.path.join(cwd,folder)):
            cwd = os.getcwd()
            continue
    return cwd

# run multiple tubes
multiple = True # set True if multiple nanotubes
if multiple:
    with open('current_2d','rb') as f:
        user_args = pickle.load(f)
else:
    # run a specific sheet
    user_args = ['MoS2','MoOTe']
    
# input for run
prototype, material = user_args
# default folder structures
default = [prototype,material,'2DSheet']
def_elec = default + ['electronic_properties']
def_hse = default + ['hse']

# get the needed folders
vasp_rx_folder = get_wd(default + ['vasp_rx'])
electronic_folder = get_wd(def_elec)
hse_folder = get_wd(def_hse)
bandstructure_folder = get_wd(def_elec + ['bandstructure'])
bandgap_folder = get_wd(def_elec + ['bandgap'])
dos_folder = get_wd(def_elec + ['dos'])
band_edge_folder = get_wd(def_elec + ['band_edge_parameters'])
# alternating sheets
alt_1_folder = get_wd([prototype, material, '2D_Sheet_alternating_1', 'vasp_rx'])
alt_2_folder = get_wd([prototype, material, '2D_Sheet_alternating_2', 'vasp_rx'])
def create_tasks():
    j1 = task('nts.relax_2D_vasp@8:6h', folder = vasp_rx_folder)
    j2 = task('nts.electronic_2D_vasp@8:2d', folder = electronic_folder, deps = [j1])
    j3 = task('nts.HSE_2D_vasp@24:1d', folder = hse_folder)
    j4 = task('nts.relax_2D_vasp@8:6h', folder=alt_1_folder, deps=[j1])
    j5 = task('nts.relax_2D_vasp@8:6h', folder=alt_2_folder, deps=[j1])
    tasks = [j1, j4, j5] #,j2]
    return tasks
