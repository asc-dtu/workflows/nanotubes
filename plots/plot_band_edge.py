import pylab as plt
import pickle
import sys
import numpy as np
from ase.db import connect

user_args = ['MoS2','MoS2','armchair',6,0]
prototype = user_args[0]
material = user_args[1]
kind = user_args[2]
n = user_args[3]
c = user_args[4]
name = prototype + '_' + material + '_' + kind + '_' + str(n) + '_' + str(c) +'_' + 'nm'

#Connect to database
db = connect('/home/energy/aegm/Nanotubes.db')
row = db.get(name=name)

band_edge = row.data.band_edge
distances = band_edge['distance']
potential = band_edge['potential']
positions = band_edge['positions']
ef = band_edge['ef']
Ev = band_edge['Ev']
Ec = band_edge['Ec']


#should be modified to assign a unique colour to each atom type. Currently this just assigns a unique color to each atom.
cmap = plt.get_cmap('viridis')
colors = cmap(np.linspace(0, 1, len(positions)))


plt.plot(distances, potential, label = 'Averaged effective potential')

if row.type == 'semiconductor':
    plt.plot([0,distances[-1]], [Ev, Ev], '--', color = 'black', label = 'Valence band edge')
    plt.plot([0,distances[-1]], [Ec, Ec], '--', color = 'black', label = 'Conduction band edge')
elif row.type == 'metal':
    plt.plot(distances, [ef, ef], '--', color = 'black', label = 'Fermi level')

for i in range(len(positions)):
    plt.axvline(positions[i][0], label = positions[i][1], color = colors[i], linestyle = '--', linewidth = 1)

plt.ylabel('Averaged effective potential [eV]')
plt.xlabel('Distance from tube center [Å]')
plt.xlim((min(distances), max(distances)))
plt.legend()
plt.show()
