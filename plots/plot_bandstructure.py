import pylab as plt
import pickle
import numpy as np
from ase.db import connect

user_args = ['MoS2','MoS2','armchair', 12, 0]
prototype = user_args[0]
material = user_args[1]
kind = user_args[2]
n = user_args[3]
c = user_args[4]
name = prototype + '_' + material + '_' + kind + '_' + str(n) + '_' + str(c) +'_' + 'nm'

#plot options
spin_orbit = False #only works for nanotube
plot_nanotube = False
plot_ribbon = True
emin = -3
emax = 3

#Connect to database
db = connect('/home/energy/aegm/Bands.db')
row = db.get(name=name)

#Plot band structure of chosen Nanotube
if plot_nanotube == True:
    
    bandstructure = row.data.bandstructure
    reference = bandstructure['reference']
    kpoints = bandstructure['kpts'][:,-1]
    energies = bandstructure['energies'] - reference
    nbands = len(energies[0,2,:])

    for i in range(nbands):
        plt.plot(kpoints,energies[0,:,i], color='green')


#Plot band structure of ribbon corresponding to chosen nanotube
if plot_ribbon == True:

    bandstructure_ribbon = row.data.bandstructure_ribbon
    reference = bandstructure_ribbon['reference']
    energies = bandstructure_ribbon['energies'] 
    kpoints = bandstructure_ribbon['kpts']
    n_unitcells = bandstructure_ribbon['n_unitcells']
    npoints = bandstructure_ribbon['npoints']
    
    nbands = n_unitcells * len(energies[0,0,:])
    if kind == 'armchair':
        kpoints = kpoints[:,0]
    elif kind == 'zigzag':
        kpoints = kpoints[:,1]
    kpoints_reduced = []
    energies_reduced = np.empty([npoints,nbands])

    for i in range(npoints):
        kpoints_reduced=np.append(kpoints_reduced,kpoints[i])
        indices = kpoints==kpoints[i]
        energies_same_k = energies[0,indices,:]
        energies_reduced[i,:] = energies_same_k.flatten()


    for i in range(nbands):
        plt.plot(kpoints_reduced,energies_reduced[:,i],'--', color='black')



#Finalize plot
plt.xlim((0.0, 0.5))
plt.ylim((emin,emax))
plt.ylabel('Energy [eV]')
plt.show()
plt.savefig('test.png')
