import pylab as plt
import pickle
import re
from ase.db import connect


user_args = ['MoS2','MoS2','armchair',6,0]
prototype = user_args[0]
material = user_args[1]
kind = user_args[2]
n = user_args[3]
c = user_args[4]
elements = re.findall('[A-Z][a-z]|[A-Z]', material)
name = prototype + '_' + material + '_' + kind + '_' + str(n) + '_' + str(c) +'_' + 'nm'

#Plot options
shift = 60
emin = -10
emax = 10
ymin = -1
ymax = 100


#Connect to database
db = connect('/home/energy/aegm/Nanotubes.db')
row = db.get(name=name)

dos = row.data.dos
energies = dos['energies']
DOS = dos['dos']
band_edge = row.data.band_edge
ef = band_edge['ef']
Ev = band_edge['Ev']
Ec = band_edge['Ec']

plt.plot(energies, DOS, label = 'DOS')

for i in 'spd':
   for j in elements:
       plt.plot(energies, dos['pdos_'+j+'-'+i]+shift, label = 'pdos_'+j+'-'+i)

if row.type == 'semiconductor':
    plt.axvline(Ev, label = 'Valence band edge', color = 'black', linestyle = '--', linewidth = 1)
    plt.axvline(Ec, label = 'Conduction band edge', color = 'black', linestyle = '--', linewidth = 1)
elif row.type == 'metal':
    plt.axvline(ef, label = 'Fermi level', color = 'black', linestyle = '--', linewidth = 1)


plt.xlim(emin,emax)
plt.ylim(ymin,ymax)
plt.xlabel('Energy [eV]')
plt.ylabel('DOS [a.u.]')
plt.legend()
plt.show()



 
