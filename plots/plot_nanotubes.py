# viusalize some tubes

from ase.db import connect

import numpy as np
import matplotlib.pyplot as plt
from operator import itemgetter
import pandas as pd
import seaborn as sns
#adjust fontsize
plt.rcParams.update({'font.size': 20})



# ----------------------------   CLASSES   ------------------------------------
class TubePlot:
    ''' Tube plotting class to not pass on all the specifications all the time'''
    def __init__(self):
        self.materials = []
        self.prototypes = []
        self.kinds = []
        self.c = 0
        self.ns_arm = []
        self.ns_zig = []
        self.strains = []
        self.stabilities = []
        self.bandgaps = []
        
    def plot_strain(self):
        strains = self.strain
        
        self.plot_prop_over_d(strains)
        plt.xlabel(r'tube radius [$\AA$]')
        plt.ylabel(r'$E_{strain}$ [eV/atom]')
        plt.legend(loc =4)
        #plt.savefig('Strain_MoS2_scaled.png',dpi =300,bbox_inches = 'tight')
    
    def plot_bandgap(self):
        
        bandgaps = self.bandgaps
        plt.figure(figsize = (12,8))
        plt.hist([float(x) for x in bandgaps[:,1]],bins = int(len(bandgaps)/5))
        plt.xlabel('Indirect bandgap [eV]')
        plt.savefig('bandgaps_all_scaled.png',dpi =300,bbox_inches = 'tight')
        
        self.plot_prop_over_d(bandgaps,TwoD = True)
        # also plot 2D for comparison
        plt.xlabel(r'tube radius [$\AA$]')
        plt.ylabel('Indirect bandgap [eV/atom]')
        plt.legend(loc =4)
    
    def plot_prop_over_d(self,prop,TwoD = False):
        '''plot a spefific property over the diameter/radius of the tube
        input as: [radius,property,mat] where mat is string as
        mat = prototype_material'''
        plt.figure(figsize = (12,8))
        min_val = 0
        max_val = []
        for prototype in self.prototypes:
            for material in self.materials:
                for kind in self.kinds:
                    mat = '{0}_{1}'.format(prototype,material)
                    plot_list = np.array([a for a in prop if mat in a])
                    try:
                        if min([float(x) for x in plot_list[:,1]]) < min_val:
                            min_val = min([float(x) for x in plot_list[:,1]])
                        max_val.append(max([float(x) for x in plot_list[:,1]]))
                    except IndexError:
                        continue
                    plt.plot([float(x) for x in plot_list[:,0]],[float(x) for x in plot_list[:,1]],'o-', label = '{} + {}'.format(mat,kind))
                    if TwoD:
                        print(mat)
                        row2d = self.db2.get(name='{0}_2D'.format(mat))
                        try:
                            gap = row2d.data["bandgap"]["indirect_gap"][0]
                        except KeyError:
                            print(mat)
                            continue
                        plt.axhline(y = gap,ls = '--',c='k')
                        max_val.append(gap)
                        plt.text(20,gap + 0.01,mat)
        plt.axhline(y=0,color = 'k')
        plt.grid()
        axes = plt.gca()
        axes.set_ylim([min_val - 0.02,max(max_val) + 0.02])
        axes.set_xlim([0,40])
    
    def plot_two_props(self):
        ''' for now just convex hull stability over strain energy
        just takes the minimum energy from convex hull as best 
        mat = str: prototype_material'''
        prop1 = np.array(self.stability)
        prop2 = np.array(self.strain)
        prop = list(zip(prop1[:,1],prop2[:,1],prop2[:,-1]))
        plt.figure(figsize = (12,8))
        if 1: # plot both prototypes
            for prototype in self.prototypes:
                for material in self.materials:
                    for kind in self.kinds:
                        mat = '{0}_{1}'.format(prototype,material)
                        plot_list = [a for a in prop if mat in a]
                        try:
                            plot_list = sorted(plot_list,key = itemgetter(0))[0]
                            plot_list = np.array(plot_list)
                        except IndexError:
                            continue
                        if 1:#not 'O' in material:
                            plt.plot(float(plot_list[0]),float(plot_list[1]),marker = 'o',ls = '', label = '{} + {}'.format(mat,kind))
                        # put text on data points
                        if 1:# float(plot_list[0]) < -100 and not 'O' in material:
                            plt.text(float(plot_list[0])+0.002,float(plot_list[1]),mat)
        
        #plt.axis('equal')
        #plt.title('Materials MX (X=SSe,STe)')
        plt.axhline(y=0,color = 'k')
        plt.axvline(x=0,color='k')
        plt.grid()
        
    def plot_stability(self):
        ''' stability list as [radius,convexHullStab,HeatofForm,mat]
        histograms show values for ALL materials in the database, not only
        specific one chosen'''
        stabilities = self.stability
        plt.figure(figsize = (12,8))
        series = pd.Series([float(x) for x in stabilities[:,1]],
                            name = 'Convex Hull stability [eV]')
        sns.distplot(series)
        if 0:
            plt.figure(figsize = (12,8))
            plt.hist([float(x) for x in stabilities[:,1]],bins = int(len(stabilities)/5))
            plt.ylabel('Convex Hull stability [eV]')
        
        plt.figure(figsize = (12,8))
        series = pd.Series([float(x) for x in stabilities[:,2]],
                            name = 'Heat of formation [eV]')
        sns.distplot(series)
        
        if 0:
            plt.figure(figsize = (12,8))
            plt.hist([float(x) for x in stabilities[:,2]],bins=int(len(stabilities)/5))
            plt.ylabel('Heat of formation [eV]')
        
        self.plot_prop_over_d(stabilities)
        plt.xlabel(r'tube radius [$\AA$]')
        plt.ylabel(r'$E_{CH}$ [eV/atom]')
        plt.legend(loc =4)
        
        self.plot_two_props()
        plt.xlabel(r'$E_{CH}$ [eV/atom]')
        plt.ylabel(r'$E_{strain}$ [eV/atom]')
    
    def get_data_from_db(self,strain = False,stability=False,bandgap = False):
        '''just collects all the data possible first, then specific materials 
        can be plotted'''
        
        strains,stabilities,bandgaps = [],[],[]
        materials = []
        for row in self.db1.select():
            materials.append(row.name.split('_')[1])
        materials = list(set(materials))
        self.materials = materials
    
        for prototype in prototypes:
            for material in materials:
                for kind in kinds:
                    if kind == 'armchair':
                        ns = ns_arm
                    elif kind == 'zigzag':
                        ns = ns_zig
                    for n in ns:
                        name = '{0}_{1}_{2}_{3}_{4}_nm'.format(prototype,material,kind,n,c)
                        try:
                            row = self.db1.get(name=name)
                            radius = row.diameter/2
                            if strain:
                                atoms = self.db1.get_atoms(name=name)
                                E_strain = (row.energy/len(atoms)) - (self.db2.get(name='{0}_{1}_2D'.format(prototype,material)).energy/len(self.db2.get_atoms(name='{0}_{1}_2D'.format(prototype,material))))
                                mat = '{0}_{1}'.format(prototype,material)
                                strains.append([radius,E_strain,kind,mat])
                            if stability:
                                stabilities.append([radius,float(row.e_ch),float(row.HoForm),mat])
                            if bandgap:
                                bandgaps.append([radius,row.data["bandgap"]["indirect_gap"][0],mat])
                        except KeyError:
                            continue
                        except AttributeError: #in database but just reserved
                            continue
                        
        self.strain = strains
        self.stability = np.array(stabilities)
        self.bandgaps = np.array(bandgaps)

# retrieve all of the data first
prototypes = ['MoS2','CdI2']
kinds = ['armchair']
c = 0
ns_arm = [6,8,10,12,14]
ns_zig = [6,8,10,12,14,16,18,20,22,24]
  
# connect to databases
db1 = connect('./test2.db') # nanotubes database
db2 = connect('./test_2D.db') # 2D materials

TubePl = TubePlot()
TubePl.ns_arm = ns_arm
TubePl.kinds = kinds
TubePl.prototypes =prototypes
TubePl.c = 0
TubePl.db1 = db1 # db1 is the nanotube database
TubePl.db2 = db2 # db2 is the corresponding 2D material database

TubePl.get_data_from_db(strain = True,stability = True,bandgap=True)

# pick some specific materials to plot
materials = []
if 1: # activate that function to only look at specific materials
    for row in TubePl.db1.select():
            n = row.name.split('_')[1]
            if 'Nb' in n: # select which material to plot
                materials.append(row.name.split('_')[1])    
    materials = list(set(materials))
    TubePl.materials = materials
TubePl.kinds = kinds
TubePl.prototypes = prototypes # prototypes # or just choose specific one ['MoS2']
TubePl.plot_strain()
TubePl.plot_stability()
TubePl.plot_bandgap()