''' 
    Script for extracting all bulk structures needed from the materialsproject
    database using only the most stable ternary combinations on the convex hull
    
    Was made for ternary structure like Mo-S-Se, but works for any combination
    such as binary, ternary ....
                                                                            '''

import pymatgen as mg
from pymatgen.entries.compatibility import MaterialsProjectCompatibility
from pymatgen.ext.matproj import MPRester
from pymatgen.analysis.phase_diagram import PhaseDiagram, PDPlotter
from pymatgen.io.ase import AseAtomsAdaptor

from ase.db import connect

import re

### USEFUL PYMATGEN FUNCTIONS
adapt = AseAtomsAdaptor()

### CONNECT TO DATABASE THAT WILL STORE STRUCTURES
db = connect('MPReference.db')

### NEEDED TERNARY ELEMENTAL COMBINATIONS
metals = ['Ti','Ge','Mo','Zr','W','Ta','Nb','Sn','V','Fe','Hf']
groups = ['OS','OSe','OTe','STe','SSe']
materials = [metal + group for metal in metals for group in groups]
metals_augfel = ['Bi','Sb','As']
groups_felix = ['SCl','SBr','SI','SeCl','SeBr','SeI','TeCl','TeBr','TeI']
groups_august = ['ClS','ClSe','ClTe','BrS','BrSe','BrTe','IS','ISe','ITe']
metals_ivano = ['Zr','Hf','Mo','Nb','Ta','Ti','V','W']
groups_ivano = ['ClBr','ClI','BrI']
materials = materials + \
            [metal + group for metal in metals_augfel for group in groups_august] + \
            [metal + group for metal in metals_augfel for group in groups_felix] + \
            [metal + group for metal in metals_ivano for group in groups_ivano]
ELEMENTARY_COMBINATIONS = [re.findall('[A-Z][^A-Z]*', mat_string) for mat_string in materials]

# insert personal key from pymatgen website here
# do all queueries in one "login-session"
with MPRester("your-MPAPI-KEY-inHere") as m:
    # queuery over all combinations, probably better way is to use the MPRester.query()
    # method since it will automatically break down requests in chunks
    # I do break the queueries automatically down here, so hopefully connection
    # will not break down
    for elementary_combination in ELEMENTARY_COMBINATIONS:
        print(elementary_combination)
        mp_entries = m.get_entries_in_chemsys(elementary_combination)
        entries = mp_entries
        
        # Process entries using the MaterialsProjectCompatibility
        compat = MaterialsProjectCompatibility()
        entries = compat.process_entries(entries)
    
        # Extract only most stable entries on convex hull
        pd = PhaseDiagram(entries)
        stableEntries = list(pd.stable_entries)
        
        for stableEntry in stableEntries:
            ### COLLECT DATA FOR ENTRY
            # data is distributed over different object, i.e. structures, entries
            # and data, all can be accesses through the MPRester
            # stableEntry is an entry data object
            mp_id = stableEntry.entry_id # unique MP-ID
            # check if already in database
            try:
                db.get(materialsproject_id = mp_id)
                continue
            except KeyError:
                pass # doesnt exist in database yet, make a new entry
                
            data = m.get_data(mp_id) # "data" - data dictionary
            structure = m.get_structure_by_material_id(mp_id) # structure data object
            
            ### EXTRACT DATA
            name = stableEntry.name # reduced formula
            print(' CURRENT MATERIAL: ', name)
            total_magnetization = data[0]['total_magnetization']
            try:
                magmoms = structure.site_properties['magmom']
            except KeyError:
                print('NO MAGMOMGS PER SITE FOR:', name)
                magmoms = dict()
                
            elasticity = data[0]['elasticity']
            parameters = stableEntry.parameters # includes Hubbard corrections and PseudoPotentials
            uncorrected_energy = stableEntry.uncorrected_energy
            total_energy = data[0]['energy']
            energy_per_atom = data[0]['energy_per_atom']
            
            atoms = adapt.get_atoms(structure) # create ASE atoms object
            db.write(atoms, name = name,materialsproject_id = mp_id, data = {
                                                 'total_magnetization':total_magnetization,
                                                 'parameters_mp': parameters,
                                                 'uncorrected_energy_mp':uncorrected_energy,
                                                 'total_energy_mp': total_energy,
                                                 'energy_per_atom_mp':energy_per_atom,
                                                 'magmoms_per_site':magmoms,
                                                 'elasticity':elasticity})
