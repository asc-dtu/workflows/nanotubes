from ase.db import connect
from ase.io import read, write
import pickle
import os
from glob import *
from nanotube_tools import *
from ase.calculators.vasp import Vasp
from ase.calculators.vasp import Vasp2
from ase.calculators.vasp import VaspDos
from ase.dft.bandgap import bandgap
from pathlib import Path

prototypes = ['MoS2', 'CdI2']


#Go into folder structure
prevdir = '../'

for prototype in prototypes:
    os.chdir(prototype)
    for material in os.listdir():
        try:
            os.chdir(material + '/2DSheet')
            if os.path.isfile('vasp_rx/relax_2D_vasp.py.done') and os.path.isfile('vasp_rx/OUTCAR'):               
                
                os.chdir('electronic_properties')
                if os.path.isfile('electronic_2D_vasp.py.done'):

                    calc_vasp2 = Vasp2(restart = True)
                    bs = calc_vasp2.band_structure()
                    bandstructure = bs.todict()
                    energies = bs.energies
                    nocc = int(calc_vasp2.get_number_of_electrons()/2)
                    Ev = energies[0,:,nocc-1].max()
                    Ec = energies[0,:,nocc].min()

                    if Ec > Ev: #semiconductor
                        efermi = (Ev + Ec)/2 #In this case place Fermi level in the middle of the gap
                        state = 'semiconductor'
                    else:
                        efermi = calc_vasp2.get_fermi_level()
                        state = 'metal' #In this case we trust the Vasp Fermi level
                        Ec = None
                        Ev = None

                    bandstructure['reference'] = efermi
                    direct_gap = bandgap(calc_vasp2, direct=True, efermi = efermi)
                    indirect_gap = bandgap(calc_vasp2, efermi = efermi)
                    bg = {}
                    bg['direct_gap'] = direct_gap
                    bg['indirect_gap'] = indirect_gap
                        
                    band_edge_parameters = {}
                    band_edge_parameters['efermi'] = efermi
                    band_edge_parameters['Ev'] = Ev
                    band_edge_parameters['Ec'] = Ec
                    band_edge_parameters['type'] = state

                    ### Save .pickle files ###
                    with open('bandstructure/bandstructure.pickle', 'wb') as f:
                        pickle.dump(bandstructure, f)

                    with open('bandgap/bandgap.pickle', 'wb') as f:
                        pickle.dump(bg, f)

                    try:
                        os.mkdir('band_edge_parameters')
                    except FileExistsError:
                        pass

                    with open('band_edge_parameters/band_edge.pickle', 'wb') as f:
                        pickle.dump(band_edge_parameters,f)

                os.chdir(prevdir)
            os.chdir(prevdir)
            os.chdir(prevdir)

        except FileNotFoundError:
            pass

            

    os.chdir(prevdir)


