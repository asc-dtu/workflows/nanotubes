from pathlib import Path
import os,sys
from nanotube_tools import hexagonal_to_orthorhombic,prototype_to_2dmaterial
from ase.io import read, write
from ase.db import connect
from ase.calculators.vasp import Vasp
from ase.optimize.bfgs import BFGS
from ase.constraints import FixedLine
import pickle
from ase.visualize import view
import re
from ase import Atom
import numpy as np
from vasp_helper import get_parameters


def get_2d_atoms_from_db(prototype,formula,c2db):
    for magstate in ['NM','FM','AFM']:
        try:
            atoms_2d = c2db.get_atoms(prototype = prototype, formula = formula,magstate=magstate)
            return atoms_2d
        except KeyError:
            continue
    raise KeyError('No structure for {} - {}'.format(prototype,formula))

#connect to database
c2db = connect('/home/energy/ivca/Nanotubes_project/c2db.db')

# get information from folder name
cwd = os.getcwd()
properties = cwd.split('/') 
prototype = properties[-7]
material = properties[-6]
kind = properties[-5]
n,c = int(properties[-4].split('_')[0]),int(properties[-4].split('_')[1])
elements = re.findall('[A-Z][a-z]|[A-Z]', material)

# define also DFT input in order to be consistent
ecut = 550

# VASP specific
isif = 2
ibrion = 2
parameters = get_parameters()

if len(re.findall('[A-Z][^A-Z]*', material)) > 2: # assumes it is janus structure for now
    el_m,el_i,el_o = re.findall('[A-Z][^A-Z]*',material)
    formula = ''.join([el_m,el_i,'2'])
    atoms_2d = get_2d_atoms_from_db(prototype,formula,c2db)
    atoms_2d = prototype_to_2dmaterial(atoms_2d,el_m,el_i,el_o)
else:
    atoms_2d = get_2d_atoms_from_db(prototype,formula,c2db)
    
#Convert to orthorhombic if unit cell hexagonal
if prototype in ['MoS2', 'CdI2', 'BN', 'MoSSe']:
    atoms_2d = hexagonal_to_orthorhombic(atoms_2d)

if kind == 'armchair':
    atoms = atoms_2d.repeat([1,n,1])
    atoms.center(vacuum=8, axis = [1,2])
    kpoints = [8,1,1] # since vacuum in y and z direction
    direction = (1,0,0) #Line along which atoms are allowed to relax

if kind == 'zigzag':
    atoms = atoms_2d.repeat([n,1,1])
    atoms.center(vacuum = 8, axis = [0,2])
    kpoints = [1,6,1] # vacuum in x and z, same kpoints as for tube
    direction = (0,1,0)


# Fix atoms to move along specified direction (1,0,0) (armchair) or (0,1,0) (zigzag)
constraints = []
for atom in atoms:
    constraint = FixedLine(atom.index, direction)
    constraints.append(constraint)
atoms.set_constraint(constraints)

calc_vasp = Vasp(xc='pbe',
                 encut=ecut,
                 ismear=0,
                 sigma=0.1,
                 ncore = 24,
                 isif = isif,
                 kpts=kpoints,
                 nsw = 500,
                 ediff = 0.00001,
                 ediffg = -0.01,
                 ibrion = ibrion,
                 gamma = True,
                 **parameters)

atoms.set_calculator(calc_vasp)

# relax the atoms
atoms.get_potential_energy()

