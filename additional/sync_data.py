''' sync data with parent folder'''
import subprocess

def sync_files():
    '''Returns shell command to sync all files from calc to parent directory'''

    cmd = 'rsync -am -u' # m for avoiding creating empty directories
    cmd += ' --exclude="*.py"'
    cmd += ' --exclude="*.py~"' # exlcude anything that is not wanted
    cmd += ' --exclude="CHG"'
    cmd += ' --exclude="CHGCAR"'
    cmd += ' --exclude="WAVECAR"'
    cmd += ' /home/energy/feltbo/Nanotubes/main_vasp/' # calculation directory - FIX THIS TO YOUR FOLDER TO SYNC!!!!
    cmd += ' /home/energy/ivca/Nanotubes_project/Nanotubes_data/' # parent data directory

    return cmd

def sync_only_OUTCAR_pickle():
    '''Returns shell command to sync all files from calc to parent directory

       This command only copies OUTCAR_O files and all *.pickle files, i.e. 
       all information needed for buidling the database!                '''

    cmd = 'rsync -am -u' # mtag = create no emtpy directories
    cmd += ' --include="*/"' # all directories
    cmd += ' --include="*.pickle"' # all .pickle files
    cmd += ' --include="OUTCAR_0"' # only initial OUTCARS
    cmd += ' --include="OUTCAR"' # also for old calculations the OUTCAR file
    cmd += ' --include="relax_tube_vasp.py.done"'
    cmd += ' --include="relax_2D_vasp.py.done"'
    cmd += ' --exclude="*"' # everything that is not explicitely specified
    cmd += ' /home/energy/ivca/Nanotubes_project/ivca_winter_calculations/' # calculation directory - FIX THIS TO YOUR FOLDER TO SYNC!!!!
    cmd += ' /home/energy/feltbo/Nanotubes/nanotube_data_only_OUTCAR_pickle/' # parent data directory

    return cmd

sync_cmd = sync_only_OUTCAR_pickle()
subprocess.run(sync_cmd,shell=True)
