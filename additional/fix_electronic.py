from ase.db import connect
from ase.io import read, write
import pickle
import os
from glob import *
from nanotube_tools import *
from stability_vasp import get_stabilities
from ase.dft.bandgap import bandgap
from ase.calculators.vasp import Vasp, Vasp2, VaspDos

prototypes = ['MoS2','CdI2']



#Go into folder structure
prevdir = '../'

for prototype in prototypes:
    os.chdir(prototype)
    for material in os.listdir():
        print(material)
        os.chdir(material)
        for kind in os.listdir():
            if kind == '2DSheet': #Skip the folder containing 2D data
                continue
            os.chdir(kind)
            for n_m in os.listdir():
                try:
                    os.chdir(n_m+'/nm')
                    if os.path.isfile('vasp_rx/relax_tube_vasp.py.done') and os.path.isfile('vasp_rx/OUTCAR'):               
                        os.chdir('electronic_properties')
                        if os.path.isfile('electronic_vasp.py.done'):
                        
                            calc_vasp2 = Vasp2(restart = True)
                            bs = calc_vasp2.band_structure()
                            bandstructure = bs.todict()
                            energies = bs.energies
                            nocc = int(calc_vasp2.get_number_of_electrons()/2)
                            Ev = energies[0,:,nocc-1].max()
                            Ec = energies[0,:,nocc].min()
                            if Ec > Ev: #semiconductor
                                efermi = (Ev + Ec)/2 #In this case place Fermi level in the middle of the gap
                                state = 'semiconductor'
                            else:
                                efermi = calc_vasp2.get_fermi_level()
                                state = 'metal' #In this case we trust the Vasp Fermi level
                                Ec = None
                                Ev = None

                            bandstructure['reference'] = efermi #correct reference to be the middle of the band gap
                            direct_gap = bandgap(calc_vasp2, direct=True, efermi = efermi)
                            indirect_gap = bandgap(calc_vasp2, efermi = efermi)
                            bg = {}
                            bg['direct_gap'] = direct_gap
                            bg['indirect_gap'] = indirect_gap


                            atoms = read('OUTCAR')
                            calc_vasp = Vasp(restart = True)
                            orbitals = ['s', 'px', 'py', 'pz', 'dxy', 'dxz', 'dyz', 'dx2','dz2']
                            DOS = VaspDos(doscar = 'DOSCAR')
                            energy = DOS.energy
                            total_dos = DOS.dos
                            integrated_dos = DOS.integrated_dos
                            dos = {}
                            dos['energy'] = energy
                            dos['dos'] = total_dos
                            dos['integrated_dos'] = integrated_dos
                            dos['orbitals'] = orbitals
                            elements = set(atoms.get_chemical_symbols())
                            for orbital in orbitals:
                                for element in elements:
                                    pdos = 0
                                    for atom in atoms:
                                        if atom.symbol == element:
                                            pdos = pdos + DOS.site_dos(atom.index, orbital)
                                            dos[orbital + '_' + element] = pdos


                            with open('band_edge_parameters/band_edge.pickle', 'rb') as f:
                                band_edge_parameters = pickle.load(f)

                            band_edge_parameters['Ev'] = Ev
                            band_edge_parameters['Ec'] = Ec
                            band_edge_parameters['efermi'] = efermi
                            band_edge_parameters['type'] = state
                            
                        
                            ### Save .pickle files ###
                            with open('bandstructure/bandstructure.pickle', 'wb') as f:
                                pickle.dump(bandstructure, f)

                            with open('bandgap/bandgap.pickle', 'wb') as f:
                                pickle.dump(bg, f)

                            with open('dos/dos.pickle', 'wb') as f:
                                pickle.dump(dos, f)

                            with open('band_edge_parameters/band_edge.pickle', 'wb') as f:
                                pickle.dump(band_edge_parameters, f)
                        os.chdir(prevdir)

                    os.chdir(prevdir)
                    os.chdir(prevdir)

                except FileNotFoundError:
                    pass

            os.chdir(prevdir)
        os.chdir(prevdir)
    os.chdir(prevdir)


