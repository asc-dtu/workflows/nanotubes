# class that places adsorbates automatically on surfaces
# MASSIVE work in progress !!!
import numpy as np

from ase.neighborlist import neighbor_list
from ase.data import covalent_radii, atomic_numbers

import pymatgen

class PlaceAdsorbateAutomatically:
    ''' Class for placing adsorbates on surfaces '''
    
    def __init__(self):
        self.atoms = []
        self.adsorbate = 'Adsorbate String'
        self.height_tol = 1 # how much surface atoms can deviate
        self.tb_tol = 1 # tolerance for distinguishing in same plane or not !
        self.surf_n = [0,0,1] ###HARD CODED SURFACE NORMAL !!! choose your own
        self.neigh_cutoff = 2.7
        self.surface_atom_indices = []
        self.top_atom_indices = []
        self.bot_atom_indices = []
        self.n_surf_atoms = 2 # how many surface atoms !? Consider both sides of slabs !!!
        self.sim_tol = 10e-10 # similarity criteria for structurally similar adsorption sites - harsh defualt criteria
        
    def find_surface_atoms(self,method1 = False, method2 = False,
                           method3 = False, verbose = False):
        '''Return surface atoms that will be considered for placing adsorbates 
        
        Initializes surface normal as surf_n = [0,0,1] - z-direction!'''
        
        ###METHOD 1: height above surface normal
        # surface normal is defined, an additional criterion defines down to which
        # depth an atom is still considered to be a surface atoms
        if method1:
        
            ps = self.atoms.get_positions()
            heights =  [np.linalg.norm(p*self.surf_n) for p in ps]
            max_h = max(heights)
            min_h = min(heights)
            
            top_surf_indices = [i for i,atom in enumerate(ps)
                                if np.sqrt((heights[i] - max_h)**2) < self.height_tol]
            
            bot_surf_indices = [i for i,atom in enumerate(ps)
                                if np.sqrt((heights[i] - min_h)**2) < self.height_tol]
            
            print("TOP: ", top_surf_indices)
            print("BOTTOM: ", bot_surf_indices)
            
            self.surface_atom_indices = np.array(top_surf_indices + bot_surf_indices)
            self.n_surf_atoms = len(self.surface_atom_indices)
            
        ### METHOD 2: HANDMADE NEIGHBOR LIST
        if method2:
            i,j,d = neighbor_list('ijd', self.atoms, cutoff = self.neigh_cutoff) # cutoff
            unique, counts = np.unique(i, return_counts=True)
            
            if verbose:
                for a,n in zip(unique,counts):
                    print("Atom {} has {} neighbors".format(a,n))
                
            min_neighbors = np.array(sorted(zip(counts,unique)))
            self.surface_atom_indices = min_neighbors[:,1][:self.n_surf_atoms]
            
        ### METHOD 3: VORONOI COORDINATION NUMBER
        if method3:
            from pymatgen.io.ase import AseAtomsAdaptor
            from pymatgen.analysis import local_env
            from pymatgen.analysis import structure_analyzer
            
            min_neighbors = list()
            adapt = AseAtomsAdaptor()
            structure = adapt.get_structure(atoms)
            #vnn = local_env.VoronoiNN()
            vnn = structure_analyzer.VoronoiAnalyzer()
            # returns:
            # voronoi index of n: <c3,c4,c6,c6,c7,c8,c9,c10>
            # where c_i denotes number of facets with i vertices.
            #Since the face count is equal to the number of nearest neighbors 
            # of a particle, this quantity is also called the coordination number
            for atom in self.atoms:
                vnn_indices = vnn.analyze(structure,n=atom.index)
                n_neighbors = sum(vnn_indices)
                if verbose:
                    print("Atom {} has {} neighbors".format(atom.index,n_neighbors))
                min_neighbors.append([n_neighbors, atom.index])
            min_neighbors = np.array(sorted(min_neighbors))

            self.surface_atom_indices = min_neighbors[:,1][:self.n_surf_atoms]
            self.n_surf_atoms = len(self.surface_atom_indices)
            
    def separate_top_bottom(self):
        ''' Function that catergorizes surface atoms into different surface 
        Make it work for 2D now since I will base it on the surface normal  '''
        
        top_surf_ind = list()
        bot_surf_ind = list()
        surf_ps = self.atoms.get_positions()*self.surf_n
        surf_ps = [np.linalg.norm(i) for i in surf_ps]
        min_surf_p = min(surf_ps)
        max_surf_p = max(surf_ps)
        for index in self.surface_atom_indices:
            if np.sqrt((surf_ps[index] - min_surf_p)**2) < self.tb_tol:
                bot_surf_ind.append(index)
            if np.sqrt((surf_ps[index] - max_surf_p)**2) < self.tb_tol:
                top_surf_ind.append(index)
        
        self.top_atom_indices = np.array(top_surf_ind)
        self.bot_atom_indices = np.array(bot_surf_ind)
        self.top_surf_height = max_surf_p
        self.bot_surf_height = min_surf_p
        
    def find_sites(self, ontop=True, bridge = True, hollow = True):
        ''' finds all sites based on delaunay triangulation
        
        option is given for
        -- ontop surface atom
        -- bridge
        -- hollow
        set false if not wanted '''
        
        origin = np.array([0,0]) # origin of coordinate system
        ox, oy = origin
        
        if ontop:
            # only append unique ontop sites
            tris = self.delaunay_tris
            NTs,T,D = tris.shape # D = DIMENSION, T = TRIANGLE POINTS, NTs = number of triangles
            ontop_sites = tris.reshape(NTs*T, D)
            # extract unique points
            ontop_sites = np.unique(ontop_sites, axis=0)
            
            print('ONTOP SITES:',ontop_sites)
            self.ontop_sites = np.array(ontop_sites)
            
        if bridge:
            # now find all bridge points: midpoint between two edge points of triangles
            bridge_sites = []
            triangle_combos = [[0,1],[0,2],[1,2]] # all connection combinations for lines
            for triangle in self.delaunay_tris:
                for i1,i2 in triangle_combos:
                    bridge_sites.append((origin + triangle[i1] + \
                                          origin + triangle[i2] )/2)
            
            # only return unique points
            bridge_sites = np.unique(bridge_sites, axis=0)
            self.bridge_sites = np.array(bridge_sites)
        
        if hollow:
            # hollow sites are calculated as centroid of each triangle
            hollow_sites = []
            for triangle in self.delaunay_tris:
                centroid = (1/3) * np.sum(triangle,axis=0)
                hollow_sites.append(centroid)
            
            self.hollow_sites = np.array(hollow_sites)
    
    def delaunay_triangulation(self,surf_indices, plot=False, 
                               offset = (0,0)):
        from scipy.spatial import Delaunay
        ''' returns the delaunay_triangulation given the edge points 
        indices: surface atoms indices, choose surface to investigate  '''
        
        # first we need to repeat the atoms to get sufficient points
        rep_indices = surf_indices
        temp_atoms = self.atoms.copy()
        temp_atoms = temp_atoms.repeat([2,2,1])
        # now I get new surface atoms, since repeated
        for i in range(3): # three new tagged surface atoms
            rep_indices = np.append(rep_indices,
                             surf_indices + ((i+1)*len(self.atoms)))
        print('HALLLOOOOOOOOOOOOO')
        print(rep_indices)
        points = list()
        for ind in rep_indices:
            points.append(temp_atoms[ind].position[:2])
        points = np.array(points)
        print(points)
        tri = Delaunay(points)
        
        if plot:
            import matplotlib.pyplot as plt
            plt.rcParams.update({'font.size': 14})
            from ase.visualize.plot import plot_atoms
            fig, ax = plt.subplots(figsize = (8,8))
            plot_atoms(temp_atoms, ax, radii=0.3 ,rotation=('0x,0y,0z'),
                       offset=offset)
            plt.plot(points[:,0], points[:,1], 'o')
            plt.plot(self.bridge_sites[:,0],self.bridge_sites[:,1], '*', c = 'r',
                         markersize = 12, label = 'bridge')
            plt.plot(self.ontop_sites[:,0],self.ontop_sites[:,1],
                     'o', c = 'green', markeredgewidth = 4,
                     markersize = 15, mfc = 'None', label = 'ontop')
            plt.plot(self.hollow_sites[:,0],self.hollow_sites[:,1],
                    's', c = 'r', markersize = 12, label = 'hollow')
            plt.triplot(points[:,0], points[:,1], tri.simplices.copy())
            ax.set_xlim([min(points[:,0]) - 1,max(points[:,0] + 1)])
            ax.set_ylim([min(points[:,1]) - 1,max(points[:,1] + 1)])
            plt.legend()
            
        self.delaunay_tris = points[tri.simplices]
        
    def extract_unique_sites(self):
        '''
        Extracts unqiue sites given a list of x,y,z positions for the adsorbate
        
        Uses the Matminer Structure Matcher fingerprint right now '''
        
        ### STRUCTURE MATCHER

        ### FIRST IMPORT SOME LIBRARIES FROM PYMATGEN
        from pymatgen.io.ase import AseAtomsAdaptor
        from pymatgen.analysis import structure_analyzer
        from pymatgen.analysis import structure_matcher
        from matminer.featurizers.site import CrystalNNFingerprint
        from matminer.featurizers.structure import SiteStatsFingerprint
        
        adapt = AseAtomsAdaptor()
        structure = adapt.get_structure(self.atoms)
        ### REDUCE SIMILARITY CRITERION
        structure_score = list()
        structure_list = list()
        ads_dict_atoms = dict()
        
        for site_group in ['ontop_sites','bridge_sites','hollow_sites']:
            
            # check if sites exist
            if not hasattr(self, site_group):
                continue
            
            for site in getattr(self, site_group):
                top_el = 'Te'
                ads_height = covalent_radii[atomic_numbers[self.adsorbate]] + \
                             covalent_radii[atomic_numbers[top_el]]
                            
                atoms.append(Atom(self.adsorbate, np.append(site,
                                    Autoshit.top_surf_height + ads_height)))
                atoms.wrap()
                structure = adapt.get_structure(atoms)
                
                # Calculate structure fingerprints. Matminer is used here
                # Further information: https://doi.org/10.3389/fmats.2017.00034
                ssf = SiteStatsFingerprint(
                      CrystalNNFingerprint.from_preset('ops', distance_cutoffs=None, x_diff_weight=0),
                      stats=('mean', 'std_dev', 'minimum', 'maximum'))
                v_score = np.array(ssf.featurize(structure))
                
                if len(structure_list) == 0:
                    structure_list.append(structure)
                    structure_score.append(v_score)
                    # append to a dictionary
                    ads_dict_atoms[site_group] = self.pmstructure_to_atoms(structure)
                else:
                    similar = False
                    for i,s in enumerate(structure_list):
                        # calculate if any structure match by user specified
                        # distance tolerance criteria
                        if np.linalg.norm(structure_score[i]-v_score) < Autoshit.sim_tol:
                            # then structures are similar and should be discarded
                            # break out of loop here
                            similar = True
                    if not similar:
                        structure_list.append(structure)
                        structure_score.append(v_score)
                        # append to dictionary, make sure not to overwrite all
                        if site_group in ads_dict_atoms:
                            obj = self.pmstructure_to_atoms(structure)
                            ads_dict_atoms[site_group].append('string works!?')
                        else:
                            # create new array for this site group
                            ads_dict_atoms[site_group] = self.pmstructure_to_atoms(structure)
            
                del atoms[-1]
                
        # last step to convert pymatgen atoms object into ASE atoms object
        print('-----------------------------------------------------------------')
        print(structure_list)
        structure_list = [self.pmstructure_to_atoms(s) for s in structure_list]
        self.adsorption_atoms_list = structure_list
        self.ads_dict_atoms = ads_dict_atoms
        
        
    def pmstructure_to_atoms(self,s):
        ''' Reads in a pymatgen structure object and returns an ase atoms object'''
        from ase import Atoms
        from pymatgen.io.ase import AseAtomsAdaptor
        
        adapt = AseAtomsAdaptor()
        temp = adapt.get_atoms(s)
        # now create the ASE atoms object
        atoms = Atoms(symbols= temp.get_chemical_formula(),
                  positions= temp.get_positions(),
                  pbc = temp.pbc,
                  cell = temp.get_cell())
        atoms.set_chemical_symbols(temp.get_chemical_symbols())
        atoms.wrap()
        
        return atoms
        
        
### SCRIPTING PART
from ase.io import read
from ase.visualize import view
from ase import Atom
atoms = read('OUTCAR_BASELINE')
adsorbate = 'Mg'
#atoms = atoms.repeat([2,2,1])
#view(atoms)
Autoshit = PlaceAdsorbateAutomatically()
Autoshit.adsorbate = adsorbate
Autoshit.atoms = atoms
Autoshit.height_tol = 1
Autoshit.find_surface_atoms(method3=True, verbose = True)
Autoshit.separate_top_bottom()
print(Autoshit.n_surf_atoms)
print(Autoshit.surface_atom_indices)
print("BOTOOM : ", Autoshit.bot_atom_indices)
print("TOP : ", Autoshit.top_atom_indices)
Autoshit.delaunay_triangulation(Autoshit.top_atom_indices,plot=False)
Autoshit.find_sites()
Autoshit.delaunay_triangulation(Autoshit.top_atom_indices,
                                plot=True, offset=(-2.25,0.35))



adapt = AseAtomsAdaptor()
structure = adapt.get_structure(atoms)
### REDUCE SIMILARITY CRITERION
Autoshit.sim_tol = 10e-10
Autoshit.extract_unique_sites()

import sys
sys.exit()


structure_score = list()
structure_list = list()
#temp_atoms = Autoshit.atoms.copy()
#temp_atoms = temp_atoms.repeat([2,2,1])
for site in Autoshit.hollow_sites:
    print(" SITE NOW: ",site)
#np.concatenate((Autoshit.ontop_sites, Autoshit.bridge_sites,
#                   Autoshit.hollow_sites), axis=0):
    top_el = 'Te'
    ads_height = covalent_radii[atomic_numbers[adsorbate]] + \
                 covalent_radii[atomic_numbers[top_el]]
                
    atoms.append(Atom(adsorbate, np.append(site,
                                      Autoshit.top_surf_height + ads_height)))
    atoms.wrap()
    structure = adapt.get_structure(atoms)
    # Calculate structure fingerprints.
    ssf = SiteStatsFingerprint(
          CrystalNNFingerprint.from_preset('ops', distance_cutoffs=None, x_diff_weight=0),
          stats=('mean', 'std_dev', 'minimum', 'maximum'))
    v_score = np.array(ssf.featurize(structure))
    
    if len(structure_list) == 0:
        structure_list.append(structure)
        structure_score.append(v_score)
    else:
        sim = False
        for i,s in enumerate(structure_list):
            print("DIFFERENCE IN SIMILARITY IS: {} against structure {}".format(np.linalg.norm(structure_score[i]-v_score), i))
            # calculate if any structure match by user specified
            # distance tolerance criteria
            if np.linalg.norm(structure_score[i]-v_score) < Autoshit.sim_tol:
                print("TOLERANCE REACHED FOR STRUCTURE {}".format(i))
                # then structures are similar and should be discarded
                # break out of loop here
                sim = True
        if not sim:
            structure_list.append(structure)
            structure_score.append(v_score)

    del atoms[-1]


structure_list = [pmstructure_to_atoms(s) for s in structure_list]
for s in structure_list:
    view(s)
import sys
sys.exit()


#from ase.lattice.cubic import FaceCenteredCubic
#slab = FaceCenteredCubic('Au', size=(2, 2, 2))

# test some here
from ase import build
#atoms = build.mx2(formula='MoSe2', kind='2H', a=3.18, 
#              thickness=3.19, size=(1, 1, 1), vacuum=8)
atoms = build.bcc111('Au', (1,2,4), a=4, 
                         vacuum=8, orthogonal=True)
view(atoms)
Auto = PlaceAdsorbateAutomatically()
Auto.n_surf_atoms = 4 # there are 2 ontop and bottom now
Auto.atoms = atoms
Auto.find_surface_atoms(method3=True, verbose = True)
Auto.separate_top_bottom()
print("BOTOOM : ", Autoshit.bot_atom_indices)
print("TOP : ", Autoshit.top_atom_indices)
Auto.delaunay_triangulation(Autoshit.top_atom_indices,plot=False)
Auto.find_sites()
Auto.delaunay_triangulation(Autoshit.top_atom_indices,
                                plot=True, offset=(-0.5,-0.5))



