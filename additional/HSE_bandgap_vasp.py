from ase.calculators.vasp import Vasp2
from ase.calculators.vasp import Vasp
from ase.dft.kpoints import *
from ase.calculators.vasp import VaspDos
from ase.io import read,write
import pickle
from ase.dft.bandgap import bandgap
import os
from scipy import interpolate
from ase.calculators.vasp import VaspChargeDensity
from ase.calculators.calculator import kptdensity2monkhorstpack
from math import sqrt
from vasp_helper import get_parameters
import numpy as np
from pathlib import Path
from shutil import copyfile



if not Path('../vasp_rx/relax_tube_vasp.py.done').is_file():
    raise FileNotFoundError('Something went wrong with the relaxation')
atoms = read('../vasp_rx/OUTCAR')

# Get information from folder structures
cwd = os.getcwd()
properties = cwd.split('/')
kind = properties[-4]


# Get parameters for calculation
parameters = get_parameters()
if kind == 'zigzag':
    kpoints = [1,1,4] # use more for armchair
else:
    kpoints = [1,1,4]


# Self-consistent calculation
parameters = get_parameters()
calc = Vasp2(xc='hse06',
                  encut=550,
                  ismear=0,
                  sigma=0.1,
                  ncore = 24,
                  kpts= kpoints,
                  nsw = 0,
                  ediff = 0.001,
                  lhfcalc = True,
                  ldiag = True,
                  hfscreen = 0.2,
                  aexx = 0.25,
                  algo = 'Damped',
                  gamma = True,
                  lcharg = True,
                  lwave = False,
                  **parameters)

atoms.set_calculator(calc)
atoms.get_potential_energy()




