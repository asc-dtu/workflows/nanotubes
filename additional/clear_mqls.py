# script that lets you delete jobs from mqueue list
import subprocess
import sys

# give string sequence that appears in the row that should be deleted
# e.g. str_remove = 'done' -> all lines with 'done' will be deleted
# e.g. str_remove '~/Nanotubes/MoS2' all calculations in that path will be deleted
str_remove = sys.argv[1]
#get all the jobs in the current folder from shell output
# kick out first and last two rows
jobs = str(subprocess.check_output('mq ls',shell = True)).split('\\n')[2:-3]
job_ids = list()
print('----------Jobs to be deleted----------')
for job in jobs:
    if str_remove in job:
        job_sep = job.split() # all the information
        print(job_sep)
        job_ids.append(job_sep[0])

i = input("Enter 'leave' to abort or press Enter to delete jobs: ")
if not i:
    subprocess.call('mq delete -i {}'.format(','.join(job_ids)),shell = True)
else:
    print("nothing has been deleted")
        
        
    


