from ase.calculators.vasp import Vasp,Vasp2,VaspDos
from ase.dft.kpoints import *
from ase.io import read,write
import pickle
from ase.dft.bandgap import bandgap
from vasp_helper import get_parameters
from shutil import copyfile
from ase.calculators.calculator import kptdensity2monkhorstpack
from pathlib import Path


### Check that relaxed structure exists
if not Path('../vasp_rx/relax_2D_vasp.py.done').is_file():
    raise FileNotFoundError('Something went wrong with the relaxation')

#Then read atoms object from relaxation
atoms = read('../vasp_rx/OUTCAR')


# Check if non-scf calculation for band structure has been done. Otherwise do one
if not Path('OUTCAR').is_file() or not Path('electronic_2D_vasp.py.done').is_file():

    # Get parameters for calculation
    parameters = get_parameters()

    calc =  Vasp2(xc='pbe',
            command='mpiexec vasp_ncl', 
             encut=550,
             ismear=0,
             sigma=0.1,
             ncore = 8,
             algo = 'Normal',
             kpts= [8,8,1],
             nsw = 0,
             ediff = 0.00001,
             ediffg = -0.01,
             ibrion = -1,
             ispin = 2,
             lorbit = 12,
             gamma = True,
             lwave = False, 
             lcharg = True,
             lmaxmix = 4,
             **parameters)

    atoms.set_initial_magnetic_moments(np.array([1 for i in range(len(atoms))]))
    atoms.set_calculator(calc)
    atoms.get_potential_energy()

    import sys
    sys.exit()

    #Next do a non-scf band structure calculation with these parameters
    emptybands = 50
    nbands = int(calc.get_number_of_electrons()) + emptybands
    encut = 550
    npoints = 48
    G = ibz_points['hexagonal']['Gamma']
    M = ibz_points['hexagonal']['M']
    K = ibz_points['hexagonal']['K']
    kpoints, x, X = get_bandpath([G,M,K,G], atoms.cell,npoints)
    labels = ['G','M','K','G']


    #Do non-selfconsistent calculation for band structure
    calc = Vasp2(xc = 'pbe',
            command='mpiexec vasp_ncl', 
            encut = encut, 
            kpts = kpoints,
            kpts_nintersections = npoints,
            reciprocal = True,
            icharg = 11,
            isif = None,
            nsw = 0,
            ibrion = -1,
            ispin = 2,
            isym = -1,
            lcharg = False,
            lwave = False,
            lorbit = 12,
            lsorbit = True,
            **parameters)

    calc.input_params.update({'saxis': '0 0 1'})
    atoms.set_calculator(calc)  
    atoms.get_potential_energy()



### Band Structure ###
calc_vasp2 = Vasp2(restart = True)
bs = calc_vasp2.band_structure()
bandstructure = bs.todict()
energies = bs.energies
nocc = int(calc_vasp2.get_number_of_electrons()/2)
Ev = energies[0,:,nocc-1].max()
Ec = energies[0,:,nocc].min()
if Ec > Ev: #semiconductor
    efermi = (Ev + Ec)/2 #In this case place Fermi level in the middle of the gap
    state = 'semiconductor'
else:
    efermi = calc_vasp2.get_fermi_level()
    state = 'metal' #In this case we trust the Vasp Fermi level
    Ec = None
    Ev = None
bandstructure['reference'] = efermi #correct reference to be the middle of the band gap


### Band Gap ###
direct_gap = bandgap(calc_vasp2, direct=True, efermi = efermi)
indirect_gap = bandgap(calc_vasp2, efermi = efermi)
bandgap = {}
bandgap['direct_gap'] = direct_gap
bandgap['indirect_gap'] = indirect_gap


#### DOS ####
calc_vasp = Vasp(restart = True)
orbitals = ['s', 'px', 'py', 'pz', 'dxy', 'dxz', 'dyz', 'dx2','dz2']
DOS = VaspDos(doscar = 'DOSCAR')
energy = DOS.energy
total_dos = DOS.dos
integrated_dos = DOS.integrated_dos
dos = {}
dos['energy'] = energy
dos['dos'] = total_dos
dos['integrated_dos'] = integrated_dos
dos['orbitals'] = orbitals
elements = set(atoms.get_chemical_symbols())
for orbital in orbitals:
    for element in elements:
        pdos = 0
        for atom in atoms:
            if atom.symbol == element:
                pdos = pdos + DOS.site_dos(atom.index, orbital)
        dos[orbital + '_' + element] = pdos

## Band Edge parameters
band_edge_parameters = {}
band_edge_parameters['efermi'] = efermi
band_edge_parameteres['Ev'] = Ev
band_edge_parameters['Ec'] = Ec
band_edge_parameters['type'] = state

# Finally save all dictionaries to pickle files

with open('bandstructure/bandstructure.pickle', 'wb') as f:
    pickle.dump(bandstructure, f)

with open('bandgap/bandgap.pickle', 'wb') as f:
    pickle.dump(bandgap, f)

with open('dos/dos.pickle', 'wb') as f:
    pickle.dump(dos, f)

with open('band_edge_parameters/band_edge.pickle', 'wb') as f:
    pickle.dump(band_edge_parameters,f)
