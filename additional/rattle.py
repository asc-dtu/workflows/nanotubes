from gpaw import *
from ase.calculators.calculator import kptdensity2monkhorstpack
from ase.io import read, write
from os import sys
from ase.db import connect
from ase.optimize.bfgs import BFGS
from ase.constraints import UnitCellFilter
from ase.visualize import view
from math import sqrt


filename = sys.argv[1]

# Calculator input (should max the parameters for used to relaxed the structure in question!)
kpoints = [1, 1, 6]
ecut = 500

#Read relaxed atom configuration - one to be kept the same and one to be rattled
atoms_old = read('{0}.gpw'.format(filename))
atoms_new = read('{0}.gpw'.format(filename))
atoms_new.rattle(stdev=0.01,seed=42)

#Setup new calculator object and perform same relaxation
calc = GPAW(mode=PW(ecut),xc='PBE', txt= None,kpts={'size': kpoints, 'gamma': True}, occupations=FermiDirac(width=0.05))
atoms_new.set_calculator(calc)
sf = UnitCellFilter(atoms_new,mask=[0,0,1,0,0,0])
opt = BFGS(sf,trajectory = '{0}_Rattled.traj'.format(filename))
opt.run(fmax=0.05)

#For loop to sum the squared distances between the atoms in the old and rattled configuration
distance_squared = 0
for i in range(len(atoms_old)):
    distance_squared = distance_squared + (atoms_old[i].x - atoms_new[i].x)**2 + (atoms_old[i].y - atoms_new[i].y)**2 + (atoms_old[i].z - atoms_new[i].z)**2

#Calculate Root mean square of distances between atoms in old and rattled configuration.
RMS = sqrt(distance_squared/len(atoms_old)) #This should be below some threshold before the structures are considered the same.
print(RMS) 
